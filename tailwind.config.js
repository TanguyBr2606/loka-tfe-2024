import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';

/** @type {import('tailwindcss').Config} */
export default {
    content : [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],
    safelist: [
        'bg-blue-300', 'bg-green-300', 'bg-red-300',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['montserrat', 'Figtree', ...defaultTheme.fontFamily.sans],
            },
            colors    : {
                primary  : '#8C379E',
                secondary: '#62929e',
                third    : '#FCC8B2',
                colorForm: 'rgba(140, 55, 158, 0.25)',
                button   : '#0D6EFD'
            }
        },
    },

    plugins: [
        forms,
        require('@tailwindcss/aspect-ratio'),
        require('daisyui'),
    ],
};
