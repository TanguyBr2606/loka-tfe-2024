@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }}
    {!! $attributes->merge(['class' => 'block w-full mt-1 p-2 border border-secondary rounded-lg']) !!}>
