<x-app-layout>
    @section('title')
        {{ 'Inscription' }}
    @endsection

    @if(session('success'))
        <div class="alert alert-success" role="alert">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                 viewBox="0 0 24 24">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                      d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
            </svg>
            <span>{{session('success')}}</span>
        </div>
    @endif
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div role="alert" class="alert alert-error">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                     viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"/>
                </svg>
                <span> {{ $error }}</span>
            </div>
        @endforeach
    @endif
    @section('content')
        <div class="container mx-auto px-4">
            <div class="flex justify-center items-center">
                <div class="md:w-10/12 lg:w-8/12 xl:w-6/12">
                    <div class="my-2 rounded-2xl lg:rounded-r-none lg:rounded-l-2xl bg-colorForm">
                        <form action="{{route('register')}}" method="post" class="p-6">
                            @csrf
                            <h1 class="text-center text-2xl lg:text-3xl font-bold">Créer votre Profil</h1>
                            <div class="grid grid-cols-1 lg:grid-cols-2 gap-4 mt-3">
                                <div>
                                    <label for="username" class="block text-sm">{{__('Username')}}
                                        <span class="text-red-500">*</span>
                                    </label>
                                    <input type="text" id="username" name="username" value="{{old('username')}}"
                                           class="w-full border border-secondary rounded-lg">
                                    <x-input-error class="mt-2" :messages="$errors->get('username')"/>
                                </div>
                                <div>
                                    <label for="email" class="block text-sm">{{__('Email')}}
                                        <span class="text-red-500">*</span>
                                    </label>
                                    <input type="email" id="email" name="email" value="{{old('email')}}"
                                           class="w-full border border-secondary rounded-lg">
                                    <x-input-error class="mt-2" :messages="$errors->get('email')"/>
                                </div>
                            </div>
                            <div class="mt-3">
                                <label for="pwd" class="block text-sm">{{__('Password')}}
                                    <span class="text-red-500">*</span>
                                </label>
                                <input type="password" id="pwd" name="password"
                                       class="w-full border border-secondary rounded-lg">
                                <x-input-error class="mt-2" :messages="$errors->get('password')"/>
                            </div>
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
                                <div>
                                    <label for="name" class="block text-sm">{{__('LastName')}}
                                        <span class="text-red-500">*</span>
                                    </label>
                                    <input type="text" id="name" name="name" value="{{old('name')}}"
                                           class="w-full border border-secondary rounded-lg">
                                    <x-input-error class="mt-2" :messages="$errors->get('name')"/>
                                </div>
                                <div>
                                    <label for="firstname" class="block text-sm">{{__('FirstName')}}
                                        <span class="text-red-500">*</span>
                                    </label>
                                    <input type="text" id="firstname" name="firstname" value="{{old('firstname')}}"
                                           class="w-full border border-secondary rounded-lg">
                                    <x-input-error class="mt-2" :messages="$errors->get('firstname')"/>
                                </div>
                            </div>
                            <div class="mt-4">
                                <label for="dateOfBirthday" class="block text-sm">{{__('dateOfBirthday')}}
                                    <span class="text-red-500">*</span>
                                </label>
                                <input type="date" id="dateOfBirthday" name="dateOfBirthday"
                                       class="w-full border border-secondary rounded-lg"
                                       value="{{ old('dateOfBirthday') }}">
                                <x-input-error :messages="$errors->get('dateOfBirthday')"/>
                            </div>
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
                                <div>
                                    <label for="phone_number" class="block text-sm">{{__('phone_number')}}</label>
                                    <input type="tel" id="phone_number" name="phone_number"
                                           value="{{old('phone_number')}}"
                                           class="w-full border border-secondary rounded-lg"
                                           title="Uniquement numéro belge ou française">
                                    <x-input-error class="mt-2" :messages="$errors->get('phone_number')"/>
                                </div>
                                <div>
                                    <label for="gsm_number" class="block text-sm">{{__('gsm_number')}}</label>
                                    <input type="text" id="gsm_number" name="gsm_number" value="{{old('gsm_number')}}"
                                           class="w-full border border-secondary rounded-lg"
                                           title="Uniquement numéro belge ou française">
                                    <x-input-error class="mt-2" :messages="$errors->get('gsm_number')"/>
                                </div>
                            </div>
                            <h2 class="text-center text-xl font-semibold mt-3 italic">{{__('Our Postal Address')}}</h2>
                            <div>
                                <label for="address" class="text-sm">{{__('Address')}}</label>
                                <input type="text" id="address" name="address"
                                       class="w-full border border-secondary rounded-lg" value="{{old('address')}}">
                                <x-input-error class="mt-2" :messages="$errors->get('address')"/>
                            </div>
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
                                <div>
                                    <label for="zip_code" class="block text-sm">{{__('zip_code')}}</label>
                                    <input type="text" id="zip_code" name="zip_code"
                                           class="w-full border border-secondary rounded-lg"
                                           value="{{old('zip_code')}}">
                                    <x-input-error class="mt-2" :messages="$errors->get('zip_code')"/>
                                </div>
                                <div>
                                    <label for="CityAddress" class="block text-sm">{{__('city')}}</label>
                                    <input type="text" id="CityAddress" name="city"
                                           class="w-full border border-secondary rounded-lg" value="{{old('city')}}">
                                    <x-input-error class="mt-2" :messages="$errors->get('city')"/>
                                </div>
                            </div>
                            <div class="mt-3">
                                <label for="country" class="block text-sm">{{__('country')}}</label>
                                <select id="country" name="country" class="w-full border border-secondary rounded-lg">
                                    <option value="#">Choisissez votre pays...</option>
                                    <option value="Belgique">Belgique</option>
                                    <option value="France">France</option>
                                </select>
                            </div>
                            <div class="mt-3">
                                <label for="terms" class="inline-flex items-center">
                                    <input id="terms" name="terms" type="checkbox" required
                                           class="rounded text-primary shadow-sm focus:ring-primary-500">
                                    <span class="ms-2 text-sm">J'accepte les
                                        <a class="font-medium" href="#">Conditions générales d'utilisation</a>
                                    </span>
                                </label>
                            </div>
                            <div class="mt-3">
                                <x-primary-button class="w-full">S'inscrire</x-primary-button>
                            </div>
                            <div class="mt-3 text-center">
                                <p>- Ou -</p>
                                <p>Vous avez déjà un compte ?
                                    <a href="{{ route('login') }}" class="text-secondary hover:font-bold">Connexion</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Image pour les écrans de bureau -->
                <div class="hidden lg:block md:w-2/12 lg:w-4/12 xl:w-6/12">
                    <img src="{{asset('images/IllustrationFormRight.png')}}" alt="illustration_Formulaire"
                         class="max-w-[699px] h-full rounded-r-2xl">
                </div>
            </div>
        </div>
    @endsection
</x-app-layout>
