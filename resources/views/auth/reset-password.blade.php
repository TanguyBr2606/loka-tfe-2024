<x-app-layout>
    @section('title')
        {{ 'Réinitialisation mot de passe' }}
    @endsection

    @section('content')
        <div class="container-fluid mx-auto px-4 py-8">
            <div class="flex flex-col justify-center items-center">
                <img src="{{asset('images/ImageFormMobile.png')}}" alt="illustration avec logo Loka Mobile"
                     class="w-[348px]">
                <div class="rounded-b-2xl bg-colorForm">
                    <div class="px-6 py-4">
                        <div class="mb-4 text-sm">
                            <p class="my-2 text-center text-lg font-semibold">{{__("Reset Password")}}</p>
                        </div>

                        <form method="POST" action="{{ route('password.store') }}">
                            @csrf
                            <!-- Password Reset Token -->
                            <input type="hidden" name="token" value="{{ $request->route('token') }}">

                            <!-- Email Address -->
                            <div class="mb-4">
                                <x-input-label for="email" :value="__('Email')"/>
                                <x-text-input id="email" type="email" name="email"
                                              :value="old('email', $request->email)" required autofocus
                                              autocomplete="username"/>
                                <x-input-error :messages="$errors->get('email')" class="mt-2"/>
                            </div>

                            <!-- Password -->
                            <div class="mt-4">
                                <x-input-label for="password" :value="__('Password')"/>
                                <x-text-input id="password" type="password" name="password" required
                                              autocomplete="new-password"/>
                                <x-input-error :messages="$errors->get('password')" class="mt-2"/>
                            </div>

                            <!-- Confirm Password -->
                            <div class="mt-4">
                                <x-input-label for="password_confirmation" :value="__('Confirm Password')"/>

                                <x-text-input id="password_confirmation" type="password"
                                              name="password_confirmation" required autocomplete="new-password"/>

                                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2"/>
                            </div>

                            <div class="flex items-center justify-end mt-4">
                                <x-primary-button>
                                    {{ __('Reset Password') }}
                                </x-primary-button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
</x-app-layout>
