<x-app-layout>
    @section('title')
        {{ 'Vérification E-mail' }}
    @endsection

    @section('content')
        <div class="container-fluid mx-auto px-4 py-8">
            <div class="flex flex-col justify-center items-center">
                <div class="rounded-2xl bg-colorForm">
                    <div class="px-6 py-4">
                        <div class="mb-4 text-sm">
                            {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
                        </div>

                        @if (session('status') == 'verification-link-sent')
                            <div class="mb-4 font-medium text-sm">
                                {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                            </div>
                        @endif

                        <div class="mt-4 flex items-center justify-between">
                            <form method="POST" action="{{ route('verification.send') }}">
                                @csrf
                                <div>
                                    <x-primary-button>{{ __('Resend Verification Email') }}</x-primary-button>
                                </div>
                            </form>

                            <a href="{{ route('logout') }}" class="underline text-sm hover:text-gray-900">
                                {{ __('Log Out') }}
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endsection
</x-app-layout>
