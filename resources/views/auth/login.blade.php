<x-app-layout>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')"/>
    @section('title')
        {{ 'Connexion' }}
    @endsection

    @section('content')
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                     viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                </svg>
                <span>{{session('success')}}</span>
            </div>
        @endif
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div role="alert" class="alert alert-error">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                         viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                    <span>{{$error}}</span>
                </div>
            @endforeach
        @endif

        <div class="container mx-auto px-2 py-2">
            <div class="flex flex-col lg:flex-row justify-center items-center">
                <div class="lg:hidden">
                    <img src="{{asset('images/ImageFormMobile.png')}}" alt="illustration avec logo Loka Mobile"
                         class="w-[406px] md:w-[502px]">
                </div>
                <!-- Image pour les écrans de bureau -->
                <div class="hidden lg:block">
                    <img src="{{asset('images/IllustrationFormLeft.png')}}" alt="illustration avec logo Loka"
                         class="lg:w-[452px] xl:w-[399px]">
                </div>
                <!-- Formulaire -->
                <div
                    class="md:w-8/12 lg:w-4/12 rounded-b-2xl lg:rounded-l-none lg:rounded-r-2xl bg-colorForm">
                    <form action="{{ route('login') }}" method="post" class="p-6">
                        @csrf
                        <div class="my-5">
                            <div class="my-5 px-5">
                                <h1 class="text-2xl md:text-3xl font-bold mb-5 text-center">Connexion à votre
                                    compte</h1>

                                <!-- Email Address -->
                                <div class="w-full mb-4">
                                    <x-input-label for="email" :value="__('Email')"/>
                                    <x-text-input id="email" type="email" name="email" :value="old('email')" required/>
                                </div>

                                <!-- Password -->
                                <div class="mb-4">
                                    <x-input-label for="password" :value="__('Password')"/>
                                    <x-text-input type="password" name="password" id="password" :value="old('password')"
                                                  required autofocus/>
                                </div>

                                <!-- Remember Me -->
                                <div class="mt-4">
                                    <label for="remember_me" class="inline-flex items-center">
                                        <input id="remember_me" name="remember" type="checkbox"
                                               class="rounded text-indigo-600 shadow-sm focus:ring-indigo-500">
                                        <span class="ms-2 text-sm">{{__('Remember me')}}</span>
                                    </label>
                                </div>

                                <!-- Connexion Button -->
                                <div class="flex justify-between mb-4">
                                    <x-primary-button class="w-full">Connexion</x-primary-button>
                                </div>

                                <!-- Request Password -->
                                <div class="flex items-center justify-start">
                                    @if (Route::has('password.request'))
                                        <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                           href="{{ route('password.request') }}">
                                            {{ __('Forgot your password?') }}
                                        </a>
                                    @endif
                                </div>

                                <!-- Register Access -->
                                <div class="mb-3 text-center">
                                    <h3 class="text-lg py-3">- Ou -</h3>
                                    <h3 class="text-lg">Créer un compte ?
                                        <a href="{{route('register')}}" class="text-secondary hover:font-bold">
                                            S'inscrire
                                        </a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    @endsection
</x-app-layout>
