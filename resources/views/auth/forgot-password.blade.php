<x-app-layout>
    @section('title')
        {{ 'Mot de passe oublié' }}
    @endsection

    @section('content')
        <div class="container mx-auto px-4 py-8">
            <div class="flex flex-col justify-center items-center">
                <img src="{{asset('images/ImageFormMobile.png')}}" alt="illustration avec logo Loka Mobile"
                     class="w-[460px] md:w-[512px]">
                <div class="max-w-lg rounded-b-2xl bg-colorForm">
                    <div class="px-6 py-4">
                        <!-- Description -->
                        <div class="mb-4 text-sm">
                            <p class="my-2 text-center text-lg font-semibold">{{ __('Forgot your password? No problem.') }}</p>
                            <p class="text-justify">{{ __('Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}</p>
                        </div>

                        <div class="text-center">
                            <p>{{__("Remember your password")}} <br>
                                <a class="text-secondary hover:font-bold"
                                   href="{{route('login')}}">{{__('Login here')}}</a>
                            </p>
                        </div>

                        <!-- Session Status -->
                        <x-auth-session-status class="mb-4" :status="session('status')"/>

                        <!-- Form -->
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <!-- Email Address -->
                            <div class="mb-4">
                                <x-input-label for="email" :value="__('Email')"/>
                                <x-text-input id="email" type="email" name="email" :value="old('email')" required
                                              autofocus/>
                                <x-input-error :messages="$errors->get('email')"/>
                            </div>

                            <!-- Submit Button -->
                            <div class="flex items-center justify-center mt-4">
                                <x-primary-button
                                    class="w-full">{{ __('Email Password Reset Link') }}</x-primary-button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    @endsection
</x-app-layout>
