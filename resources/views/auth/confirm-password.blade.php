<x-app-layout>
    @section('title')
        {{ 'Confirmation mot de passe' }}
    @endsection

    @section('content')
        <div class="container-fluid mx-auto px-4 py-8">
            <div class="flex flex-col justify-center items-center">
                <div class="rounded-2xl bg-colorForm">
                    <div class="px-6 py-4">
                        <div class="mb-4 text-sm">
                            {{ __('This is a secure area of the application. Please confirm your password before continuing.') }}
                        </div>

                        <form method="POST" action="{{ route('password.confirm') }}">
                            @csrf
                            <!-- Password -->
                            <div>
                                <x-input-label for="password" :value="__('Password')"/>
                                <x-text-input id="password" type="password" name="password" required
                                              autocomplete="current-password"/>
                                <x-input-error :messages="$errors->get('password')" class="mt-2"/>
                            </div>

                            <div class="flex justify-center mt-4">
                                <x-primary-button class="w-full">{{ __('Confirm') }}</x-primary-button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection

</x-app-layout>
