@extends('layouts.app')

@section('title')
    {{ 'Produit' }}
@endsection

@section('content')
    @if(session('success'))
        <div class="alert alert-success my-4 flex items-center mx-auto max-w-3xl" role="alert">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                 viewBox="0 0 24 24">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                      d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
            </svg>
            <p>{{ session('success') }}</p>
        </div>
    @endif

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div role="alert" class="alert alert-error my-4 flex items-center mx-auto max-w-3xl">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                     viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                </svg>
                <span>{{ $error }}</span>
            </div>
        @endforeach
    @endif
    <section class="py-10">
        <h1 class="text-3xl text-center font-semibold italic mb-8">Nos produits en Location</h1>
        <div class="mx-auto sm:px-7 lg:max-w-7xl lg:px-6">
            <div class="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-6">
                @foreach ($products as $product)
                    <div
                        class="max-w-xs mx-auto p-3 border border-secondary rounded-lg shadow-lg flex flex-col w-full">
                        @if($product->image && file_exists(public_path("/images/$product->image")))
                            <img src="images/{{$product->image}}" alt="photo {{$product->name}}"
                                 class="mx-auto mb-4 object-center object-fill h-[250px]">
                        @elseif($product->image && file_exists(public_path("/storage/$product->image")))
                            <img src="storage/{{$product->image}}" alt="photo {{$product->name}}"
                                 class="mx-auto mb-4 object-center object-fill h-[250px]">
                        @endif
                        <h3 class="text-lg font-semibold text-gray-800 mb-2 text-center">{{ $product->name }}</h3>
                        <hr class="border-t-1 border-[#A5C5C8] my-2">
                        <p class="text-gray-700 my-2">Prix de Location: {{ $product->price_caution }} €</p>
                        <p class="text-gray-700 my-2">Prix de la Caution: {{ $product->price_location }} €</p>
                        @if($product->description != null)
                            <p class="text-gray-700 my-2">Description: {{ $product->description }}</p>
                        @endif
                        <p class="text-gray-700 my-2">Disponibilité:
                            @if($product->disponibility == 1 )
                                <span class="text-green-500 font-semibold">Disponible</span>
                            @else
                                <span class="text-red-600 font-bold">Indisponible</span>
                            @endif
                        </p>
                        <div class="flex-grow"></div>
                        <div class="card-actions justify-center mt-4">
                            @auth('customer')
                                @if($product->disponibility == 1)
                                    <form action="{{route('cart.add')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <button type="submit"
                                                class="btn bg-button hover:bg-blue-800 text-white rounded-2xl">
                                            Ajouter au panier
                                        </button>
                                    </form>
                                @endif
                            @endauth
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection
