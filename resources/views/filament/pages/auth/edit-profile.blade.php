<div>
    <header class="fi-simple-header py-8">
        <h1 class="fi-header-heading text-2xl font-bold tracking-tight">
            {{__("My profile")}}
        </h1>
        <p class="fi-simple-header-subheading mt-2 text-left"></p>
    </header>

    <x-filament-panel::form wire:sumit="save">
        {{ $this->form }}
        <x-filament-panel::form.actions
            :actions="$this->getCachedFormActions()"
            :full-widht="$this->hasFullWidthFormActions()"

        />
    </x-filament-panel::form>
</div>
