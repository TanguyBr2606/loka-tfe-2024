<x-filament::page>
    <p class="text-center text-danger-600 font-semibold">
        Uniquement les demandes approuvés sont présentes sur le calendrier
    </p>
    @livewire(\App\Filament\Resources\RentalResource\Widgets\CalendarWidget::class)
</x-filament::page>
