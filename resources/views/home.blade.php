<x-app-layout>
    @section('title')
        {{ 'Accueil' }}
    @endsection

    @section('content')
        @if(session('message'))
            <div role="alert" class="alert alert-info my-4 flex items-center mx-auto max-w-3xl">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                     class="h-6 w-6 shrink-0 stroke-current">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                </svg>
                <span class="text-black">{{ session('message') }}</span>
            </div>
        @endif

        @if($errors->any())
            @foreach($errors->all() as $error)
                <div role="alert" class="alert alert-error my-4 flex items-center mx-auto max-w-3xl">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                         viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                    <span>{{$error}}</span>
                </div>
            @endforeach
        @endif

        <div class="max-w-screen-2xl mx-auto px-6">
            <!-- Hero Section -->
            <section class="text-center py-5">
                <h1 class="text-2xl sm:text-3xl md:text-4xl lg:text-5xl font-semibold my-3 italic">
                    Bienvenue chez LoKa
                </h1>
                <p class="text-lg sm:text-xl md:text-2xl lg:text-3xl my-3">
                    Location de mobilier, de matériel horeca et de décoration pour votre événement
                </p>
            </section>
            <!-- Content Section -->
            <section>
                <div class="border-t-4 border-primary border-dashed my-6 mx-auto w-full"></div>
                <div class="grid grid-cols-1 lg:grid-cols-3 gap-6">
                    <div class="lg:col-span-2">
                        <h2 class="text-xl sm:text-2xl md:text-3xl font-semibold mb-4">Loka Locations : location de
                            matériel événementiel</h2>
                        <p class="text-base sm:text-lg text-justify mb-4">Loka Locations est spécialisé dans la location
                            de matériel
                            événementiel pour tous vos événements. Nous mettons à votre disposition des tables en
                            location, des chaises en location, des mange debout en location, location matériel horeca,
                            décoration, des bars lumineux en location, de la vaisselle en location, des chapiteaux, des
                            chauffes-terrasses en location, des écrans, ... Louer chez Loka Locations, c'est s'assurer
                            de disposer d'un matériel de qualité et d'un service de location professionnel avec plus de
                            10 ans d'expérience. Nous garantissons la livraison et la reprise du matériel dans les temps
                            et en respectant vos contraintes.</p>
                        <p class="text-base sm:text-lg text-justify">Sur ce site, vous pouvez réaliser des demandes de
                            devis pour la
                            location de matériel pour banquets, soirées, réception de mariages, anniversaires, salon,
                            exposition,...</p>
                        <p class="text-base sm:text-lg mt-4 text-justify">N'hésitez pas à nous contacter si vous ne
                            trouvez pas le
                            produit recherché dans notre catalogue, nous nous efforcerons de répondre à votre demande le
                            plus rapidement possible.</p>
                    </div>
                    <div class="flex items-center justify-center lg:justify-end">
                        <div class="relative">
                            <img src="{{asset('images/accueil/photo7.jpg')}}" alt="Inspiration Image"
                                 class="rounded-xl opacity-50 w-full lg:w-[768px] h-[250px] sm:h-[400px]">
                            <div class="absolute inset-0 flex items-center justify-center">
                                <div class="flex flex-col justify-center items-center text-center">
                                    <img src="{{asset('images/accueil/logocadeaux.png')}}" alt="logo Cadeaux"
                                         class="h-[50px] sm:h-[80px] w-[50px] sm:w-[80px] mb-4">
                                    <p class="text-sm sm:text-lg text-gray-600 mb-4">BESOIN D'INSPIRATION POUR UN
                                        MARIAGE, UN
                                        BAPTÊME, UNE FÊTE D'ENTREPRISE OU UN AUTRE ÉVÉNEMENT PARTICULIER ?</p>
                                    <a href="{{route('contact.create')}}"
                                       class="bg-primary text-white px-6 py-2 rounded-2xl w-50">Contactez-nous !
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Most rental Products Section -->
            <section>
                <div class="border-t-4 border-primary border-dashed my-4 mx-auto"></div>
                <h2 class="text-xl sm:text-3xl mb-4 text-center italic">Les 4 produits les plus loués</h2>
                <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-6">
                    <div class="border border-secondary rounded-lg p-4">
                        <img src="{{asset('images/product/2_Samovar.png')}}" alt="Samovar"
                             class="mx-auto mb-4 h-[250px]">
                        <h3 class="text-lg sm:text-xl text-center mb-2">Samovar</h3>
                        <hr class="border-t-1 border-[#A5C5C8] my-2">
                        <p>Prix de Location: 20 €</p>
                        <p>Prix de la Caution: 50 €</p>
                    </div>
                    <div class="border border-secondary rounded-lg p-4">
                        <img src="{{asset('images/product/4_CuiseurPates.png')}}" alt="Cuiseur à Pâtes"
                             class="mx-auto mb-4 h-[250px]">
                        <h3 class="text-xl text-center mb-2">Cuiseur à Pâtes</h3>
                        <hr class="border-t-1 border-[#A5C5C8] my-2">
                        <p>Prix de Location: 30 €</p>
                        <p>Prix de la Caution: 60 €</p>
                    </div>
                    <div class="border border-secondary rounded-lg p-4">
                        <img src="{{asset('images/product/5_chaises.png')}}" alt="Chaise"
                             class="mx-auto mb-4 h-[250px]">
                        <h3 class="text-xl text-center">Chaise</h3>
                        <hr class="border-t-1 border-[#A5C5C8] my-2">
                        <p>Prix de Location: 1 €</p>
                        <p>Prix de la Caution: 2 €</p>
                    </div>
                    <div class="border border-secondary rounded-lg p-4">
                        <img src="{{asset('images/product/11_Appareil_Hotdog.png')}}" alt="Appareil à Hot Dog"
                             class="mx-auto mb-4 h-[250px]">
                        <h3 class="text-xl text-center mb-2">Appareil à Hot Dog</h3>
                        <hr class="border-t-1 border-[#A5C5C8] my-2">
                        <p>Prix de Location: 25 €</p>
                        <p>Prix de la Caution: 30 €</p>
                    </div>
                </div>
            </section>
            <!-- Inspirations Section -->
            <section class="py-10">
                <div class="border-t-4 border-primary border-dashed my-6 mx-auto"></div>
                <h2 class="text-lg sm:text-xl font-semibold mb-6"><i class="bi bi-dash-lg"></i> Inspirations</h2>
                <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                    <img src="{{asset('images/accueil/photo1.jpg')}}" alt="Inspiration 1"
                         class="rounded-lg w-full sm:w-[476px] h-[200px] sm:h-[358px]">
                    <img src="{{asset('images/accueil/photo2.jpg')}}" alt="Inspiration 2"
                         class="rounded-lg w-full sm:w-[476px] h-[200px] sm:h-[358px]">
                    <img src="{{asset('images/accueil/photo3.jpg')}}" alt="Inspiration 3"
                         class="rounded-lg w-full sm:w-[476px] h-[200px] sm:h-[358px]">
                    <img src="{{asset('images/accueil/photo4.jpg')}}" alt="Inspiration 4"
                         class="rounded-lg w-full sm:w-[476px] h-[200px] sm:h-[358px]">
                    <img src="{{asset('images/accueil/photo5.jpg')}}" alt="Inspiration 5"
                         class="rounded-lg w-full sm:w-[476px] h-[200px] sm:h-[358px]">
                    <img src="{{asset('images/accueil/photo6.jpg')}}" alt="Inspiration 6"
                         class="rounded-lg w-full sm:w-[476px] h-[200px] sm:h-[358px]">
                </div>
            </section>
        </div>
    @endsection
</x-app-layout>
