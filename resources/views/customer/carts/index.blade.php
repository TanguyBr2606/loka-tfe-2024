<x-app-layout>
    @section('title')
        {{ 'Panier' }}
    @endsection

    @section('content')
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                     viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                </svg>
                <span>{{session('success')}}</span>
            </div>
        @endif
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div role="alert" class="alert alert-error">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                         viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                    <span>{{$error}}</span>
                </div>
            @endforeach
        @endif
        <section class="container border border-secondary rounded-2xl mx-auto my-5">
            <div class="sm:flex my-5">
                <div class="w-full sm:w-3/4 bg-white px-8 py-10">
                    <div class="flex justify-between border-b pb-8">
                        <h1 class="font-semibold text-2xl">Votre demande</h1>
                        <h2 class="font-semibold text-2xl">{{$cartCount}} produits</h2>
                    </div>
                    @if($products->isEmpty())
                        <p class="mt-4">Votre panier est vide.</p>
                    @else
                        @foreach($products as $product)
                            <div class="md:flex items-strech py-8 md:py-10 lg:py-8 border-t border-gray-300">
                                <div class="md:w-6/12 2xl:w-1/3">
                                    @if($product['image'] && file_exists(public_path("/images/$product->image")))
                                        <img src="images/{{$product->image}}" alt="photo {{$product->name}}"
                                             class="w-3/5 h-full object-center object-cover md:block hidden">
                                    @elseif($product->image && file_exists(storage_path("app/public/$product->image")))
                                        <img src="storage/{{$product->image}}" alt="photo {{$product->name}}"
                                             class="w-40 h-full object-center object-cover md:block hidden">
                                    @endif
                                </div>
                                <div class="md:w-6/12 2xl:w-2/3 flex flex-col justify-center">
                                    <div class="flex items-center justify-between w-full">
                                        <h2 class="text-lg font-bold">{{ $product->name }}</h2>
                                        <!-- Form to update the quantity -->
                                        <form action="{{route('cart.update', $product->id)}}" method="post">
                                            @csrf
                                            <input type="number" name="quantity" value="{{ $product->quantity}}"
                                                   min="1" class="w-16 text-center border rounded-md">
                                            <button type="submit"
                                                    class="text-xs leading-3 text-blue-600 hover:underline cursor-pointer ml-3">
                                                Mettre à jour
                                            </button>
                                        </form>
                                    </div>
                                    <p class="text-s leading-3 pt-4">
                                        Prix de location unitaire: {{$product->price_location}}€
                                    </p>
                                    <p class="text-s leading-3 pt-4">Prix de Caution: {{$product->price_caution}}€</p>
                                    <div class="flex items-center justify-between pt-5">
                                        <form action="{{route('cart.destroy', $product->id)}}" method="post">
                                            @csrf
                                            <button type="submit"
                                                    class="text-xs leading-3 text-red-600 hover:underline cursor-pointer">
                                                Retirer
                                            </button>
                                        </form>
                                        <p class="text-base font-black leading-none">
                                            {{ number_format($product->price_location * $product->quantity,2) }}€
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <a href="{{route('products.index')}}" class="flex font-semibold text-primary text-sm mt-10">
                        <svg class="fill-current mr-2 text-primary w-4" viewBox="0 0 448 512">
                            <path
                                d="M134.059 296H436c6.627 0 12-5.373 12-12v-56c0-6.627-5.373-12-12-12H134.059v-46.059c0-21.382-25.851-32.09-40.971-16.971L7.029 239.029c-9.373 9.373-9.373 24.569 0 33.941l86.059 86.059c15.119 15.119 40.971 4.411 40.971-16.971V296z"/>
                        </svg>
                        Continuer ma demande
                    </a>
                </div>
                <div id="summary" class="w-full sm:w-1/4 md:w-1/2 px-8 py-10">
                    <h1 class="font-semibold text-2xl text-center border-b pb-8">
                        Résumé de ma demande
                    </h1>
                    {{--<div class="flex justify-between mt-10 mb-5">
                        <span class="font-semibold text-sm uppercase">{{$cartCount}} Produits</span>
                    </div>--}}
                    <div class="py-5">
                        <form action="{{ route('rental.store') }}" method="post">
                            @csrf
                            <div class="mb-4">
                                <x-input-label for="payment_type">Payement</x-input-label>
                                <select id="payment_type" name="payment_type"
                                        class="block p-2 w-full text-sm border border-secondary rounded-lg">
                                    <option value="enlevement">A l'enlèvement</option>
                                    <option value="virement">Par Virement Bancaire</option>
                                </select>
                            </div>
                            <div class="mb-4">
                                <x-input-label for="start_date">Date de début</x-input-label>
                                <x-text-input type="date" name="start_date" required/>
                            </div>
                            <div class="mb-4">
                                <x-input-label for="end_date">Date de début</x-input-label>
                                <x-text-input type="date" name="end_date" required/>
                            </div>
                            <div>
                                <x-input-label for="customer_comment">Commentaire éventuel</x-input-label>
                                <textarea name="customer_comment"
                                          class="w-full border border-secondary rounded-lg px-3 py-2 mt-1"></textarea>
                            </div>
                            <div class="border-t">
                                <div class="flex font-semibold justify-between py-4 text-sm uppercase">
                                    <span>Sous-Total prix de location</span>
                                    <span>{{$cartTotalLocation}} €</span>
                                </div>
                                <div class="flex font-semibold justify-between py-4 text-sm uppercase">
                                    <span>Sous-Total prix de Caution</span>
                                    <span>{{$cartTotalCaution}} €</span>
                                </div>
                                <div class="border-t"></div>
                                <div class="flex font-semibold justify-between py-6 text-sm uppercase">
                                    <span>Montant Total de la demande (TVA inclus)</span>
                                    <span>{{$cartTotal}} €</span>
                                </div>
                                <x-primary-button class="w-full">Valider ma demande</x-primary-button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    @endsection
</x-app-layout>
