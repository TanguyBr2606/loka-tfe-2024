@extends('layouts.app')

@section('title')
    {{ 'Contact' }}
@endsection

@section('content')
    @if(session('success'))
        <div class="alert alert-success" role="alert">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                 viewBox="0 0 24 24">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                      d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
            </svg>
            <span>{{session('success')}}</span>
        </div>
    @endif

    <section class="container mx-auto mt-6 px-4 lg:px-0">
        <div class="flex flex-col lg:flex-row justify-center lg:items-stretch items-center">
            <!-- Formulaire de Contact -->
            <div class="w-full lg:w-6/12 lg:mb-0 lg:h-full">
                <div
                    class="bg-colorForm border-2 border-secondary rounded-t-2xl lg:rounded-r-none lg:rounded-l-2xl xl:rounded-l-2xl p-6 h-full flex flex-col">
                    <form method="POST" action="{{ route('contact.store') }}" class="flex-grow">
                        @csrf
                        <div class="space-y-4 xl:mt-3">
                            <h1 class="text-center text-2xl font-bold mt-2">Formulaire de Contact</h1>
                            <input type="hidden" id="customer_id" name="customer_id" value="{{auth()->id()}}">
                            <div class="w-full">
                                <x-input-label for="subject" :value="__('Subject')"/>
                                <x-text-input id="subject" type="text" name="subject"/> <!--required-->
                                @error('subject')
                                <span class="text-red-600 text-sm mt-1">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                                <div>
                                    <x-input-label for="subject" :value="__('LastName')"/>
                                    <x-text-input id="subject" type="text" name="lastname" autofocus
                                                  value="{{ old('lastname', auth()->guard('customer')->user()->name ?? '') }}"/>
                                    @error('lastname')
                                    <span class="text-red-600 text-sm mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div>
                                    <x-input-label for="firstname" :value="__('FirstName')"/>
                                    <x-text-input id="subject" type="text" name="firstname" autofocus
                                                  value="{{ old('firstname', auth()->guard('customer')->user()->firstname ?? '') }}"/>
                                    @error('firstname')
                                    <span class="text-red-600 text-sm mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="w-full">
                                <x-input-label for="email" :value="__('Email')"/>
                                <x-text-input id="email" type="email" name="email"
                                              value="{{ old('email', auth()->guard('customer')->user()->email ?? '') }}"/>
                                @error('email')
                                <span class="text-red-600 text-sm mt-1">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="w-full">
                                <x-input-label for="message" :value="__('Message')"/>
                                <textarea id="message" class="block w-full mt-2 p-2 border border-secondary rounded-lg"
                                          name="message" rows="4"></textarea>
                                @error('message')
                                <span class="text-red-600 text-sm mt-1">{{ $message }}</span>
                                @enderror
                            </div>
                            <x-primary-button class="w-full">Envoyer</x-primary-button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Coordonnées -->
            <div class="w-full lg:w-6/12 lg:h-full xl:h-full mb-4 lg:mb-0">
                <div
                    class="bg-colorForm border-2 border-secondary rounded-b-2xl lg:rounded-l-none lg:rounded-r-2xl p-6 h-full flex flex-col justify-between">
                    <h2 class="text-center text-xl font-semibold underline lg:mt-3 xl:mt-0">Coordonnées</h2>
                    <div class="lg:mt-6 xl:mt-0">
                        <p class="lg:mt-3">LoKa Location</p>
                        <p class="lg:mt-3">N° de Téléphone : 082/72.45.29</p>
                        <p class="lg:mt-3">N° de Gsm : 0472/05.42.78</p>
                        <p class="lg:mt-3">Email : contact@Loka.be</p>
                        <p class="lg:mt-3 xl:mt-4">Localisé sur Beauraing</p>
                    </div>
                    <div class="flex justify-center items-center md:mt-4 lg:mt-3 xl:mt-1">
                        {{--<img src="{{ asset('images/MAP.png') }}" alt="MAP"
                             class="md:w-full xl:w-9/12 h-full rounded-2xl">--}}
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d163822.68118550224!2d4.785052079933812!3d50.09135315233157!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c1c30d32036a5d%3A0xb46f5cab75a67fb9!2sBeauraing!5e0!3m2!1sfr!2sbe!4v1732012532405!5m2!1sfr!2sbe"
                            width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
