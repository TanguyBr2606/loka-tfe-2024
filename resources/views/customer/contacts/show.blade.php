@extends('layouts.app')

@section('title')
    {{ 'Liste Messages' }}
@endsection

@section('content')
    <div class="container mx-auto py-5 h-full">
        <div class="flex justify-center items-center h-full">
            <div class="w-full md:w-10/12 lg:w-8/12 xl:w-6/12">
                <div class="bg-white shadow-lg rounded-lg p-8">
                    <h2 class="text-3xl font-bold text-center text-gray-800 mb-6">Mes Messages de Contact</h2>

                    @if ($userMessages->isEmpty())
                        <div class="text-center py-8">
                            <p class="text-lg text-gray-600">Vous n'avez aucun message de contact.</p>
                        </div>
                    @else
                        @foreach($userMessages as $message)
                            <div class="border border-gray-200 rounded-lg mb-6 p-6 bg-gray-50">
                                <div class="flex items-center justify-between mb-3">
                                    <div class="text-lg font-semibold text-gray-800">
                                        Sujet: {{$message->subject}}
                                    </div>
                                    <div class="text-sm text-gray-500 flex items-center">
                                        <x-heroicon-s-calendar class="w-5 h-5 mr-1"/>
                                        {{ $message->created_at->translatedFormat('d M Y, H:i') }}
                                    </div>
                                </div>
                                <p class="mb-4 text-gray-700">{{$message->message}}</p>
                                @if ($message->response)
                                    <div class="p-4 bg-green-100 border border-green-300 rounded-lg">
                                        <p class="font-semibold text-green-800 flex items-center">
                                            <x-heroicon-s-check-circle class="w-5 h-5 mr-2"/>
                                            Réponse: {{$message->response}}
                                        </p>
                                    </div>
                                @else
                                    <div class="p-4 bg-yellow-100 border border-yellow-300 rounded-lg">
                                        <p class="font-semibold text-yellow-800 flex items-center">
                                            <x-heroicon-s-clock class="w-5 h-5 mr-2"/>
                                            Aucune réponse pour le moment.
                                        </p>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
