@php use Illuminate\Contracts\Auth\MustVerifyEmail; @endphp
<section>
    <header>
        <h2 class="text-lg text-center font-medium">
            {{ __('Profile Information') }}
        </h2>
    </header>

    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <form method="post" action="{{ route('profile.update') }}" class="mt-6 space-y-6">
        @csrf
        @method('patch')

        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
            <div>
                <x-input-label for="username" :value="__('Username')"/>
                <x-text-input id="username" name="username" type="text" class="bg-gray-200" disabled
                              :value="old('username', $customer->username)"/>
                <x-input-error class="mt-2" :messages="$errors->get('username')"/>
            </div>
            <div>
                <x-input-label for="email" :value="__('Email')"/>
                <x-text-input id="email" name="email" type="email" :value="old('email', $customer->email)" required/>
                <x-input-error class="mt-2" :messages="$errors->get('email')"/>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
            <div>
                <x-input-label for="name" :value="__('LastName')"/>
                <x-text-input id="name" name="name" type="text" :value="old('name', $customer->name)" required
                              autofocus/>
                <x-input-error class="mt-2" :messages="$errors->get('name')"/>
            </div>
            <div>
                <x-input-label for="first_name" :value="__('FirstName')"/>
                <x-text-input id="first_name" name="firstname" type="text"
                              :value="old('firstname', $customer->firstname)" required autofocus/>
                <x-input-error class="mt-2" :messages="$errors->get('firstname')"/>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
            <div>
                <x-input-label for="phone_number" :value="__('phone_number')"/>
                <x-text-input type="text" id="phone_number" name="phone_number"
                              :value="old('phone_number', $customer->phone_number)"
                              title="Uniquement numéro belge ou française" autofocus/>
                <x-input-error class="mt-2" :messages="$errors->get('phone_number')"/>
            </div>
            <div>
                <x-input-label for="gsm_number" :value="__('gsm_number')"/>
                <x-text-input type="text" id="gsm_number" name="gsm_number"
                              :value="old('gsm_number', $customer->gsm_number)"
                              title="Uniquement numéro belge ou française" autofocus/>
                <x-input-error class="mt-2" :messages="$errors->get('gsm_number')"/>
            </div>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
            <div>
                <x-input-label for="dateOfBirthday" :value="__('dateOfBirthday')"/>
                <x-text-input type="date" id="dateOfBirthday" name="dateOfBirthday"
                              :value="old('dateOfBirthday', $customer->dateOfBirthday)"/>
                <x-input-error class="mt-2" :messages="$errors->get('dateOfBirthday')"/>
            </div>
        </div>
        <h2 class="text-lg text-center italic font-medium">
            {{ __('Our Postal Address') }}
        </h2>
        <div>
            <x-input-label for="address" :value="__('address')"/>
            <x-text-input type="text" id="address" name="address" :value="old('address', $customer->address)"/>
            <x-input-error class="mt-2" :messages="$errors->get('address')"/>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-4 mt-3">
            <div>
                <x-input-label for="zip_code" :value="__('zip_code')"/>
                <x-text-input type="text" id="zip_code" name="zip_code" :value="old('zip_code', $customer->zip_code)"/>
                <x-input-error class="mt-2" :messages="$errors->get('zip_code')"/>
            </div>
            <div>
                <x-input-label for="city" :value="__('city')"/>
                <x-text-input type="text" id="city" name="city" :value="old('city', $customer->city)"/>
                <x-input-error class="mt-2" :messages="$errors->get('city')"/>
            </div>
        </div>
        <div>
            <x-input-label for="country" :value="__('country')"/>
            <select id="country" name="country"
                    class="block w-full mt-1 p-2 border border-secondary rounded-md">
                <option value="#">Choisissez votre pays...</option>
                <option value="belgique" @if($customer->country == "belgique") selected @endif>Belgique</option>
                <option value="france" @if($customer->country == "france") selected @endif>France</option>
            </select>
        </div>
        <div>
            @if ($customer instanceof MustVerifyEmail && ! $customer->hasVerifiedEmail())
                <div>
                    <p class="text-sm mt-2">
                        {{ __('Your email address is unverified.') }}
                        <button form="send-verification" class="underline text-sm text-secondary hover:text-[#195A6A]
                        rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            {{ __('Click here to re-send the verification email.') }}
                        </button>
                    </p>
                    @if (session('status') === 'verification-link-sent')
                        <p class="mt-2 font-medium text-sm">
                            {{ __('A new verification link has been sent to your email address.') }}
                        </p>
                    @endif
                </div>
            @endif
        </div>
        <div class="flex flex-col justify-center items-center gap-4 mt-6">
            <x-primary-button>{{ __('Save') }}</x-primary-button>
            @if (session('status') === 'profile-updated')
                <p x-data="{ show: true }" x-show="show" x-transition x-init="setTimeout(() => show = false, 2000)"
                   class="text-sm text-gray-600 dark:text-gray-400">{{ __('Saved.') }}</p>
            @endif
            <a href="{{ route('profile.export') }}" class="btn btn-success px-4 py-2 rounded-2xl">
                <i class="bi bi-download"></i> Télécharger vos données au format JSON
            </a>
        </div>
    </form>
</section>
