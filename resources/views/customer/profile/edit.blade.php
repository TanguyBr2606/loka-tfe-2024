<x-app-layout>
    @section('title')
        {{ 'Profil' }}
    @endsection

    @section('content')
        <div class="py-12">
            <div class="max-w-3xl mx-auto px-4 sm:px-6 lg:px-10 space-y-6">
                <div class="p-4 sm:p-8 bg-colorForm shadow rounded-lg">
                    @include('customer.profile.partials.update-profile-information-form',$customer)
                </div>
                <div class="p-4 sm:p-8 bg-colorForm shadow rounded-lg">
                    @include('customer.profile.partials.update-password-form')
                </div>
                <div class="p-4 sm:p-8 bg-colorForm shadow rounded-lg">
                    @include('customer.profile.partials.delete-user-form')
                </div>
            </div>
        </div>
    @endsection
</x-app-layout>
