<x-app-layout>
    @section('title')
        {{ 'Demande N°' . $rental->number }}
    @endsection

    @section('content')
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                     viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                </svg>
                <span>{{session('success')}}</span>
            </div>
        @endif
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div role="alert" class="alert alert-error">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                         viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                    <span>{{$error}}</span>
                </div>
            @endforeach
        @endif
        <section class="container mx-auto px-4 md:px-6 my-5">
            <h1 class="text-xl sm:text-2xl md:text-3xl font-bold mb-4 text-center">
                Détails de la demande #{{ $rental->number }}
            </h1>

            <div class="mb-4 p-4">
                <p class="py-2"><strong>Date de début :</strong> {{ $rental->start_date->translatedFormat('d-m-Y') }}
                </p>
                <p class="py-2"><strong>Date de fin :</strong> {{ $rental->end_date->translatedFormat('d-m-Y') }}</p>
                <p class="py-2"><strong>Statut :</strong>
                    <span
                        class="inline-flex items-center px-3 py-2 rounded-2xl {{ $rental->status->getBackgroundColorClass()}}">
                        <x-dynamic-component :component="$rental->status->getIcon()" class="w-4 h-4 mr-2"/>
                        {{ $rental->status->getLabel() }}
                    </span>
                </p>
                <p class="py-2"><strong>Commentaire :</strong> {{ $rental->customer_comment ?? 'Aucun' }}</p>
                <p class="py-2"><strong>Type de Paiement choisi :</strong>
                    @if($rental->payment_type == 'enlevement')
                        A l'enlèvement
                    @elseif($rental->payment_type == 'virement')
                        Virement bancaire
                    @endif
                </p>
                @if($rental->payment_type == 'virement')
                    <p class="py-2"><strong>Communication Bancaire :</strong> {{$rental->bank_reference}} </p>
                    <p class="py-2">Payement de la caution à l'enlèvement</p>
                @endif
                @if($rental->status->getLabel() === 'Demande rejetée')
                    <div
                        class="mt-4 p-4 bg-red-50 border border-red-400 text-red-700 px-4 py-3 rounded-lg shadow-lg relative text-center"
                        role="alert">
                        <span class="absolute top-0 bottom-0 left-0 px-4 py-3">
                            <svg class="fill-current h-6 w-6 text-red-500" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 24 24">
                                <path
                                    d="M12 0a12 12 0 0112 12 12 12 0 01-12 12A12 12 0 010 12 12 12 0 0112 0zm0 22a10 10 0 100-20 10 10 0 000 20zm-1-14h2v6h-2v-6zm0 8h2v2h-2v-2z"/>
                            </svg>
                        </span>
                        <p class="text-lg font-semibold text-red-800 mb-2">Motif du rejet de la demande :</p>
                        <span class="block sm:inline">{{ $rental->admin_note ?? 'Aucun message fourni.' }}</span>
                        <span class="absolute top-0 bottom-0 right-0 px-4 py-3">
                            <svg class="fill-current h-6 w-6 text-red-500" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 24 24">
                                <path
                                    d="M12 0a12 12 0 0112 12 12 12 0 01-12 12A12 12 0 010 12 12 12 0 0112 0zm0 22a10 10 0 100-20 10 10 0 000 20zm-1-14h2v6h-2v-6zm0 8h2v2h-2v-2z"/>
                            </svg>
                        </span>
                    </div>
                @endif
            </div>

            <h2 class="text-lg sm:text-xl md:text-2xl font-bold mb-2">Produits demandés</h2>
            <!-- Pour les petits écrans -->
            <div class="block md:hidden">
                @foreach($rental->products as $product)
                    <div class="mb-4 border border-gray-200 rounded-lg p-4 shadow-sm">
                        <p><strong>Produit :</strong> {{ $product->name }}</p>
                        <p><strong>Quantité :</strong> {{ $product->pivot->quantity }}</p>
                        <p><strong>Prix de location unitaire :</strong> {{ $product->price_location }} €</p>
                        <p><strong>Prix de caution :</strong> {{ $product->price_caution }} €</p>
                    </div>
                @endforeach
                <div class="border-t border-gray-200 pt-4">
                    {{--<p class="text-right font-bold">Total Prix de location : {{$rental->total_rental_price}} €</p>
                    <p class="text-right font-bold">Total Prix de caution : {{$rental->total_deposit}} €</p>--}}
                    <p class="text-right font-bold"> Total : {{ $rental->total_rental_price + $rental->total_deposit}} €</p>
                </div>
            </div>

            <!-- Pour les écrans de taille moyenne et grandes -->
            <div class="hidden md:block overflow-x-auto">
                <table class="min-w-full shadow-md rounded-lg overflow-hidden">
                    <thead>
                    <tr class="bg-gray-200 border-b">
                        <th class="py-2 px-4 font-bold text-sm text-left">Produit</th>
                        <th class="py-2 px-4 font-bold text-sm text-left">Quantité</th>
                        <th class="py-2 px-4 font-bold text-sm text-left">Prix de location</th>
                        <th class="py-2 px-4 font-bold text-sm text-left">Prix de caution</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rental->products as $product)
                        <tr class="border-b">
                            <td class="py-2 px-4">{{ $product->name }}</td>
                            <td class="py-2 px-4">{{ $product->pivot->quantity }}</td>
                            <td class="py-2 px-4">{{ $product->price_location }} €</td>
                            <td class="py-2 px-4">{{ $product->price_caution }} €</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot class="bg-gray-100">
                    <tr class="border-t">
                        <td class="py-2 px-4 text-right font-bold" colspan="2">Total :</td>
                        <td class="py-2 px-4 text-center font-bold" colspan="2">{{ $rental->total_rental_price + $rental->total_deposit }} €</td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <div class="mt-5 flex justify-center items-center">
                <a href="{{ route('rental.index') }}"
                   class="btn bg-button text-white hover:bg-blue-800 px-4 py-2 rounded-lg shadow-md">
                    Retour à mes demandes</a>
            </div>
        </section>
    @endsection
</x-app-layout>
