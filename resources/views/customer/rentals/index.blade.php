<x-app-layout>
    @section('title')
        {{ 'Mes Demandes' }}
    @endsection

    @section('content')
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                     viewBox="0 0 24 24">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                </svg>
                <span>{{session('success')}}</span>
            </div>
        @endif
        @if($errors->any())
            @foreach($errors->all() as $error)
                <div role="alert" class="alert alert-error">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 shrink-0 stroke-current" fill="none"
                         viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                    <span>{{$error}}</span>
                </div>
            @endforeach
        @endif
        <section class="container mx-auto my-5 px-6 md:px-10">
            <h1 class="text-xl sm:text-2xl md:text-3xl font-bold mb-4 text-center">Mes demandes de location</h1>
            @if($rentals->isEmpty())
                <p class="text-center text-base sm:text-lg">Vous n'avez pas encore fait de demandes.</p>
            @else
                <div class="shadow-lg rounded-lg overflow-hidden">
                    <!-- Pour les écrans de taille moyenne et grandes -->
                    <div class="hidden md:block overflow-x-auto">
                        <table class="w-full table-auto">
                            <thead>
                            <tr class="bg-gray-100 text-center">
                                <th class="w-1/5 py-2 sm:py-4 px-2 sm:px-6 font-bold uppercase text-xs sm:text-sm md:text-base">
                                    Référence
                                </th>
                                <th class="w-1/5 py-2 sm:py-4 px-2 sm:px-6 font-bold uppercase text-xs sm:text-sm md:text-base">
                                    Date de début
                                </th>
                                <th class="w-1/5 py-2 sm:py-4 px-2 sm:px-6 font-bold uppercase text-xs sm:text-sm md:text-base">
                                    Date de fin
                                </th>
                                <th class="w-1/5 py-2 sm:py-4 px-2 sm:px-6 font-bold uppercase text-xs sm:text-sm md:text-base">
                                    Statut
                                </th>
                                <th class="w-1/5 py-2 sm:py-4 px-2 sm:px-6 font-bold uppercase text-xs sm:text-sm md:text-base">
                                    Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white">
                            @foreach($rentals as $rental)
                                <tr class="text-center">
                                    <td class="py-2 sm:py-4 px-2 sm:px-6 border-b border-gray-200">{{ $rental->number }}</td>
                                    <td class="py-2 sm:py-4 px-2 sm:px-6 border-b border-gray-200">
                                        {{ $rental->start_date->translatedFormat('d-m-Y') }}
                                    </td>
                                    <td class="py-2 sm:py-4 px-2 sm:px-6 border-b border-gray-200">
                                        {{ $rental->end_date->translatedFormat('d-m-Y') }}
                                    </td>
                                    <td class="py-2 sm:py-4 px-2 sm:px-6 border-b border-gray-200">
                                    <span
                                        class="inline-flex items-center px-3 py-2 rounded-2xl {{$rental->status->getBackgroundColorClass()}}">
                                        <x-dynamic-component :component="$rental->status->getIcon()"
                                                             class="w-5 h-5 mr-2"/>
                                        {{ $rental->status->getLabel() }}
                                    </span>
                                    </td>
                                    <td class="py-2 sm:py-4 px-2 sm:px-6 border-b border-gray-200">
                                        <a href="{{ route('rental.show', $rental->id) }}"
                                           class="text-secondary hover:underline">Voir détails</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- Pour les petits écrans -->
                    <div class="block md:hidden">
                        @foreach($rentals as $rental)
                            <div class="border border-gray-200 rounded-lg mb-4 p-4">
                                <div class="mb-2">
                                    <span class="font-bold">Référence :</span>
                                    <span># {{ $rental->id }}</span>
                                </div>
                                <div class="mb-2">
                                    <span class="font-bold">Date de début :</span>
                                    <span>{{ $rental->start_date->translatedFormat('d-m-Y') }}</span>
                                </div>
                                <div class="mb-2">
                                    <span class="font-bold">Date de fin :</span>
                                    <span>{{ $rental->end_date->translatedFormat('d-m-Y') }}</span>
                                </div>
                                <div class="mb-2">
                                    <span class="font-bold">Statut :</span>
                                    <span
                                        class="flex items-center justify-center px-3 py-2 rounded-2xl {{$rental->status->getBackgroundColorClass()}}">
                                        <x-dynamic-component :component="$rental->status->getIcon()"
                                                             class="w-5 h-5 mr-2"/>
                                        {{ $rental->status->getLabel() }}
                                    </span>
                                </div>
                                <div class="flex justify-center items-center">
                                    <a href="{{ route('rental.show', $rental->id) }}"
                                       class="text-secondary hover:underline">Voir détails</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </section>
    @endsection
</x-app-layout>
