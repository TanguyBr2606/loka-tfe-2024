<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-theme="light">
<head>
    <!-- Meta Character -->
    <meta charset="utf-8">
    <!-- Meta viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Meta Title -->
    <meta name="title" content="Location de Matériel Événementiel - Loka">
    <!-- Meta description -->
    <meta name="description"
          content="Découvrez LoKa, votre expert en location de matériel événementiel pour tous vos événements dans la région de Beauraing. Louez des équipements professionnels horeca et bien plus encore pour garantir le succès de vos événements.">
    <!-- Meta keywords -->
    <meta name="keywords"
          content="location matériel événementiel, location équipement événement, location son et lumière, location matériel pour événement, équipement événementiel">
    <!-- Token CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Site Logo -->
    <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/png"/>
    <!-- Site Title -->
    <title>@yield('title') | Loka</title>
    <!-- Scripts -->
    @vite(['resources/css/app.css'])
    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <!-- ICONS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body class="min-h-screen flex flex-col">
@include('layouts.navbar')
<main class="flex-grow">
    @yield('content')
</main>
@include('layouts.footer')
@vite(['resources/js/app.js'])
</body>
</html>
