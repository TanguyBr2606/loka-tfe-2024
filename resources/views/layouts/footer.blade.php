<footer>
    <div class="bg-third text-white py-4 px-3 mt-16">
        <div class="container mx-auto flex flex-wrap items-center justify-between">
            <div class="w-full md:w-1/4 md:text-center md:mb-0 mb-8">
                <p class="text-xs md:text-sm">Copyright 2024 &copy; Loka, Tout droits réservés</p>
            </div>
            <div class="w-full md:w-3/4 md:text-center md:mb-0 mb-8">
                <ul class="list-reset flex justify-center flex-wrap text-xs md:text-sm gap-3">
                    <li class="mx-4">
                        <a href="{{asset('pdf/Conditions_Générales_Loka.pdf')}}">
                            Conditions générales d'utilisation (CGU)
                        </a>
                    </li>
                    <li class="mx-4">
                        <a href="/admin" target="_blank">
                            Admin
                        </a>
                    </li>
                    <li class="mx-4">
                        <a href="{{asset('pdf/Conditions_Générales_de_Location_Loka.pdf')}}">
                            Conditions générales de locations (CGL)
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
