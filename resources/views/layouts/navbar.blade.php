<header class="bg-third">
    <div class="navbar px-0">
        <div class="navbar-start">
            <!-- Mobile Dropdown Menu -->
            <div class="dropdown relative">
                <div tabindex="0" role="button" class="btn btn-ghost lg:hidden" title="burgerMenu Button">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="h-6 w-6 hover:text-primary">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
                    </svg>
                </div>

                <!-- Dropdown Content -->
                <ul tabindex="0" id="mobile-menu"
                    class="menu absolute dropdown-content top-16 left-0 w-screen bg-base-100 rounded-b-lg shadow-lg hidden">
                    <li>
                        <a class="block hover:bg-primary hover:text-white px-4 py-3 rounded-lg text-center"
                           href="{{ route('home') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="block hover:bg-primary hover:text-white px-4 py-3 rounded-lg text-center"
                           href="{{ route('products.index') }}">Produit</a>
                    </li>
                    <li>
                        <a class="block hover:bg-primary hover:text-white px-4 py-3 rounded-lg text-center"
                           href="{{ route('contact.create') }}">Contact</a>
                    </li>
                    @guest('customer')
                        <li>
                            <a class="block hover:bg-primary hover:text-white px-4 py-3 rounded-lg text-center"
                               href="{{ route('login') }}">Login</a>
                        </li>
                    @endguest
                </ul>
            </div>
            <!-- Mobile Logo Centered -->
            <a href="{{ route('home') }}" class="lg:hidden">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="w-20 h-auto mx-auto">
            </a>

            <!-- Desktop Logo -->
            <a href="{{ route('home') }}" class="hidden lg:flex lg:justify-start lg:ml-4">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="md:logo w-20 h-auto">
            </a>
        </div>

        <div class="navbar-center hidden lg:flex">
            <ul class="menu menu-horizontal">
                <li><a href="{{route('home') }}">Accueil</a></li>
                <li><a href="{{route('products.index')}}">Produit</a></li>
                @guest('customer')
                    <li><a href="{{route('contact.create')}}">Contact</a></li>
                    <li><a href="{{route('login')}}">Login</a></li>
                @else
                    @auth('customer')
                        <li><a href="{{ route('contact.create') }}">Contact</a></li>
                    @endauth
                @endguest
            </ul>
        </div>
        {{--Logo Facebook--}}
        <div class="navbar-end flex items-center">
            <a class="mr-4 lg:block" href="https://www.facebook.com/LoKaLocation" target="blank"
               title="Page Facebook">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="blue" class="bi bi-facebook"
                     viewBox="0 0 16 16">
                    <path
                        d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951"/>
                </svg>
            </a>
        </div>

        @auth('customer')
            <div class="flex-none">
                <div class="dropdown dropdown-end">
                    <div tabindex="0" role="button" class="btn btn-ghost btn-circle">
                        <div class="indicator">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24"
                                 stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                      d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"/>
                            </svg>
                            <span class="badge badge-sm indicator-item">{{ $cartCount }}</span>
                        </div>
                    </div>
                    <div tabindex="0" class="card card-compact dropdown-content z-[1] mt-3 w-52 shadow bg-gray-100">
                        <div class="card-body">
                            <span class="text-lg font-bold">{{ $cartCount }} produits</span>
                            <span class="text-info">Sous-total: {{ number_format($cartTotal, 2) }} €</span>
                            <div class="card-actions">
                                <a href="{{route('cart.index')}}" class="btn btn-primary btn-block">Voir le
                                    panier</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dropdown dropdown-end">
                    <div tabindex="0" role="button" class="btn btn-ghost btn-circle avatar mr-2">
                        <div class="w-10 rounded-full">
                            <img alt="Avatar" src="{{asset('images/avatar.png')}}"/>
                        </div>
                    </div>
                    <ul tabindex="0"
                        class="menu menu-sm dropdown-content rounded-box z-[1] mt-3 w-60 p-2 shadow bg-gray-100">
                        <li>
                            <a href="{{route('profile.edit')}}" class="justify-between">Profil</a>
                        </li>
                        <li>
                            <a href="{{route('rental.index')}}" class="justify-between">Mes demandes de location</a>
                        </li>
                        <li>
                            <a href="{{route('contact.show')}}" class="justify-between">Mes demandes de contact</a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" class="text-red-600 text-bold">Déconnecter</a>
                        </li>
                    </ul>
                </div>
            </div>
        @endauth
    </div>
</header>

<script>
    document.querySelector('[role="button"]').addEventListener('click', function () {
        let menu = document.getElementById('mobile-menu');
        menu.classList.toggle('hidden');
    });
</script>
