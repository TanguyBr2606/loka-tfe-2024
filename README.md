<p align="center">
  <img src="public/images/logo.png" alt="Logo du site LoKa">
</p> 

## Description

Dans le cadre de mon travail de fin d'études, j'ai développé une application web destinée à une entreprise de location
de matériel événementiel pour améliorer sa visibilité sur Internet et leur permettre une meilleure gestion des produits 
disponibles à la location, la gestion des demandes de contact, la gestion des locations.

La plateforme est développée en utilisant **Laravel**, **Laravel Filament**, **Laravel Breeze**, **Tailwind CSS**

## Installation

### 1. **Les prérequis :**

    - PHP ^8.2 ou version ultérieure
    - MySQL ^8.0 ou version ultérieure
    - Composer ^2.7.7
    - npm ^10.8.0 ou version ultérieure
    - Une connexion internet
    - Un serveur local (wamp, mamp,...)
    - Git bash
    - Un compte Gmail
    - Mot de passe de configuration Gmail SMTP

### 2. **Copie du projet :**

2.1. Dans Git Bash, allez jusqu'à votre dossier `www` ou `htdocs` puis effectuez la commande :

```bash
git clone -b staging https://gitlab.com/TanguyBr2606/loka-tfe-2024 loka
```

2.2. Toujours via Git Bash, allez dans le projet cloné (`cd loka`) et effectuez séparément les
commandes :

```bash
composer install
```

```bash
npm install
```

(Ces installations peuvent prendre du temps sous Windows)

2.3. Ensuite, effectuez la commande :

```bash
php artisan install:project
```
et répondez aux informations demandées.
(le nom de la base de donnée et au choix. Ex : ‘Loka’).

2.4. Effectuez la commande pour installation des migrations en base de données et insertion des donneés prédéfinies.

```bash
php artisan dbmigration
 ```

Ils vous demanderont si vous voulez créer la base de données, car elle n'existe pas, répondez **"yes"**.

Si cette commande ne fonctionne pas, vous pouvez faire à la place.

```bash
php artisan migrate
 ```

```bash
php artisan db:seed
 ```

2.5. Cette commande permet de créer un lien avec le dossier où sera enregistrée la photo lors de la création d'un 
produit pour être accessible en public.

```bash
php artisan storage:link
```

Lors de l'installation des données prédéfinies, les comptes administrateurs sont créés et ont le mot de passe par
défaut : `admin`
Pour les comptes utilisateurs prédéfinis : le mot de passe est `passsword`

Les adresses mail pour les comptes admin sont :

*     Super-Admin : tanguybrondelet@outlook.be
*     Adminstrateur simple : admin@outlook.be

### 3. **configurez votre fichier .env :**

3.1. Pour le SMTP de Gmail :

   ```
   MAIL_MAILER=smtp
   MAIL_HOST=smtp.gmail.com
   MAIL_PORT=587
   MAIL_USERNAME="votre adresse email"
   MAIL_PASSWORD="votre mot de passe d'application"
   MAIL_ENCRYPTION=tls
   MAIL_FROM_ADDRESS="votre adresse email"
   MAIL_FROM_NAME="${APP_NAME}"
   ``` 

Le mot de passe pour l'application est à créer à l'adresse ci-dessous :
[Lien vers le mot de passe](https://myaccount.google.com/apppasswords)

### 4. **Démarrer les serveurs de l'application :**

4.1. Si vous désirez accéder à l’application via le serveur Laravel vous pouvez effectuer la commande suivante :

   ```bash
   npm run loka
   ``` 

Vous pouvez ensuite vous rendre sur l’url fournie à côté de **INFO Server running on.**
<br><br>
L’installation est terminée.

## Structure de l'application

Ce diagramme représente la structure logique de mon application, montrant les différents composants, leurs relations et
les flux de données entre eux. Vous pouvez le retrouver en format PDF :

![Diagramme_Application_Loka.png](Diagramme_Application_Loka.png)

Vous pouvez retrouver la description du schéma d'application ainsi que le schéma dans ces 2 fichiers pdf se trouvant
dans la racine du projet: Description schéma application - Loka.pdf & Schéma Application - Loka (diagrams.net).pdf

Ce diagramme aide à visualiser l'architecture globale de mon application et à comprendre comment ses différents
composants interagissent entre eux pour fournir les fonctionnalités nécessaires aux utilisateurs.

## Licence

Droits d'auteur 2024 **Tanguy Brondelet**

Tous droits réservés. La reproduction, la distribution, la modification ou l'utilisation de ce logiciel, en tout ou en
partie, sans l'autorisation expresse et écrite de l'auteur est strictement interdite.

Ce logiciel est fourni "tel quel", sans aucune garantie expresse ou implicite, y compris les garanties de qualité
marchande, d'adéquation à un usage particulier et d'absence de contrefaçon. En aucun cas, l'auteur ne pourra être tenu
responsable des dommages ou des réclamations découlant de l'utilisation de ce logiciel.

Pour toutes demandes ou questions, veuillez me contacter à cette adresse email suivante : tanguybrondelet@outook.be
