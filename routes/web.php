<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RentalController;
use App\Http\Middleware\XSS;
use Illuminate\Support\Facades\Route;

Route::middleware([XSS::class])->group(function () {
    require __DIR__ . '/auth.php';

    //Main Page
    Route::get('/', function () {
        return view('home');
    })->name('home');

    // Product
    Route::get('/products', [ProductController::class, 'index'])->name('products.index');
    //Route::get('/products/{product}', [ProductController::class, 'show'])->name('products.show');

    // Contact
    Route::get('/contact', [ContactController::class, 'create'])->name('contact.create');
    Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');

    Route::middleware('auth:customer')->group(function () {
        //Profile
        Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
        Route::get('export/profile', [ProfileController::class, 'export'])->name('profile.export');

        // Message
        Route::get('/contact/messages', [ContactController::class, 'show'])->name('contact.show');

        // Cart & Rental Requesting
        Route::get('/cart', [CartController::class, 'index'])->name('cart.index');
        Route::get('/cart/info', [CartController::class, 'getCartInfo'])->name('cart.info');
        Route::post('/cart/add', [CartController::class, 'addToCart'])->name('cart.add');
        Route::post('/cart/update/{product}', [CartController::class, 'updateQuantity'])->name('cart.update');
        Route::post('/cart/remove/{product}', [CartController::class, 'destroy'])->name('cart.destroy');
        Route::post('/rental/store', [RentalController::class, 'store'])->name('rental.store');
        Route::get('/rental', [RentalController::class, 'index'])->name('rental.index');
        Route::get('/rental/{id}', [RentalController::class, 'show'])->name('rental.show');
    });
});

