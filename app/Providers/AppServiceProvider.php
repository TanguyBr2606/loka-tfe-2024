<?php

namespace App\Providers;

use App\Models\Product;
use App\Models\User;
use App\Policies\UserPolicy;
use Carbon\Carbon;
use Filament\Notifications\Notification;
use Filament\Pages\Page;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\ValidationException;

class AppServiceProvider extends ServiceProvider
{
    protected array $policies
        = [
            User::class => UserPolicy::class,
        ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Page::$reportValidationErrorUsing = function (ValidationException $exception) {
            Notification::make()
                        ->title($exception->getMessage())
                        ->danger()
                        ->send();
        };

        Carbon::setLocale('fr');

        View::composer('*', function ($view) {
            $cart              = Session::get('cart', []);
            $cartCount         = 0;
            $cartTotalLocation = 0;
            $cartTotalCaution  = 0;
            $cartTotal         = 0;

            foreach ($cart as $productId => $details) {
                $product = Product::find($productId);
                if ($product) {
                    $cartCount         += $details['quantity'];
                    $cartTotalLocation += $product->price_location * $details['quantity'];
                    $cartTotalCaution  += $product->price_caution;
                }
            }

            $cartTotal = $cartTotalCaution + $cartTotalLocation;

            $view->with('cartCount', $cartCount)
                 ->with('cartTotalLocation', $cartTotalLocation)
                 ->with('cartTotalCaution', $cartTotalCaution)
                 ->with('cartTotal', $cartTotal);
        });
    }
}
