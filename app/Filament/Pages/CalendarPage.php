<?php

namespace App\Filament\Pages;

use Filament\Pages\Page;

class CalendarPage extends Page
{
    protected static ?string $navigationIcon  = 'heroicon-o-calendar';
    protected static ?string $navigationLabel = 'Calendrier des Locations';
    protected static ?string $title           = 'Calendrier des Locations';
    
    protected static string $view = 'filament.pages.calendar-page';
}
