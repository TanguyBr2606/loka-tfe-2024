<?php

namespace App\Filament\Pages\Auth;

use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Pages\Auth\EditProfile as BaseEditProfile;
use Illuminate\Support\Facades\Hash;


class EditProfile extends BaseEditProfile
{
    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('Personal information'))
                       ->schema([
                           TextInput::make('user_name')
                                    ->label(__('Username'))
                                    ->minLength(3)
                                    ->maxLength(16)
                                    ->required()
                                    ->unique(ignoreRecord: true)
                                    ->columnSpan(2),
                           TextInput::make('first_name')
                                    ->label(__('FirstName'))
                                    ->regex('^[a-zA-Z\s\',\-]+$^')
                                    ->minLength(3)
                                    ->maxLength(16)
                                    ->required()
                                    ->unique(ignoreRecord: true),
                           TextInput::make('name')
                                    ->label(__('Name'))
                                    ->regex('^[a-zA-Z\s\',\-]+$^')
                                    ->minLength(3)
                                    ->maxLength(16)
                                    ->required()
                                    ->unique(ignoreRecord: true),
                       ])->columns(2),
                Section::make(__('Contact'))
                       ->schema([
                           $this->getEmailFormComponent(),
                           TextInput::make('phone')
                                    ->label(__('phone_number'))
                                    ->tel()
                                    ->minLength(10)
                                    ->maxLength(12)
                                    ->telregex('/^\+(324[5-9][0-9]{7}|33[6-7][0-9]{8})$/'),
                           Placeholder::make(__('Message for Format number'))
                                      ->content('Uniquement numéro belge ou française'),
                       ]),
                Section::make(__('Password'))
                       ->schema([
                           TextInput::make('current_password')
                                    ->label(__('Current Password'))
                                    ->password()
                                    ->required()
                                    ->columnSpan(2)
                                    ->revealable(filament()->arePasswordsRevealable())
                                    ->dehydrated(false)
                                    ->live(debounce: 200)
                                    ->afterStateUpdated(function ($state, callable $set) {
                                        if (!$this->verifyCurrentPassword($state)) {
                                            $set('password', '');
                                            $set('passwordConfirmation', '');
                                        }
                                    }),
                           $this->getPasswordFormComponent()
                                ->visible(fn($get) => $this->verifyCurrentPassword($get('current_password')))
                                ->columnSpan(2),
                           $this->getPasswordConfirmationFormComponent()
                                ->columnSpan(2),
                       ])->columns(2)
            ]);
    }

    protected function verifyCurrentPassword($currentPassword): bool
    {
        return Hash::check($currentPassword, auth()->user()->password);
    }

}
