<?php

namespace App\Filament\Widgets;

use App\Models\Contact;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Rental;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;

class StatisticsCard extends BaseWidget
{
    protected function getStats(): array
    {
        return [
            Stat::make(__('Number of Product'), Product::count()),
            Stat::make(__('Number of Clients'), Customer::count()),
            Stat::make(__('Total rental requests received'), Rental::count()),
            Stat::make(__('Total Rental requests approved'), Rental::where('status', '=', 'approved')->count()),
            Stat::make(__('Total Rental requests pending'), Rental::where('status', '=', 'pending')->count()),
            Stat::make(__('Total Rental requests rejected'), Rental::where('status', '=', 'rejected')->count()),
            Stat::make(__('Total Contact Requests pending'), Contact::where('response', '=', null)->count()),
            Stat::make(__('Total Contact Requests received'), Contact::count()),
        ];
    }
}
