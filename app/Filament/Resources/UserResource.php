<?php

namespace App\Filament\Resources;

use App\Filament\Resources\UserResource\Pages;
use App\Models\Role;
use App\Models\User;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon  = 'heroicon-c-user';
    protected static ?string $navigationGroup = 'Admin Users';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('Personal Information'))
                       ->schema([
                           TextInput::make('user_name')
                                    ->label(__('Username'))
                                    ->required()
                                    ->unique(ignoreRecord: true)
                                    ->columnSpan(2)
                                    ->disabled(fn(?User $record) => $record != null),
                           TextInput::make('first_name')
                                    ->label(__('FirstName'))
                                    ->regex('^[a-zA-Z\s\',\-]+$^')
                                    ->required(),
                           TextInput::make('name')
                                    ->label(__('LastName'))
                                    ->regex('^[a-zA-Z\s\',\-]+$^')
                                    ->required(),
                           Section::make(__('Contact Information'))
                                  ->schema([
                                      TextInput::make('email')
                                               ->label(__('Email'))
                                               ->email()
                                               ->unique(ignoreRecord: true)
                                               ->required(),
                                      TextInput::make('password')
                                               ->label(__('Password'))
                                               ->password()
                                               ->required()
                                               ->revealable(filament()->arePasswordsRevealable())
                                               ->hidden(fn(?User $record) => $record != null),
                                      TextInput::make('phone_number')
                                               ->label(__('phone_number'))
                                               ->tel()
                                               ->regex('/^\+(32[1-9][0-9]{7}|33[1-9][0-9]{8})$/'),
                                      TextInput::make('gsm_number')
                                               ->label(__('gsm_number'))
                                               ->tel()
                                               ->regex('/^\+(324[5-9][0-9]{7}|33[6-7][0-9]{8})$/'),
                                      Placeholder::make(__('Message for Format number'))
                                                 ->content('Uniquement numéro belge ou française'),
                                  ])->columns(2),
                           Section::make(__('Role'))
                                  ->schema([
                                      TextInput::make('roles.name')
                                               ->label(__('Rôle'))
                                               ->formatStateUsing(fn($state, $record) => $record?->roles()
                                                                                                ->first()->name)
                                               ->disabled()
                                               ->hidden(fn($record) => !$record || !$record->hasRole('super-administrateur')),
                                      // Champ de sélection pour les autres utilisateurs
                                      Select::make('role')
                                            ->label(__('Rôle'))
                                            ->relationship('roles', 'name')
                                            ->options(Role::where('name', '!=', 'super-administrateur')
                                                          ->pluck('name', 'id'))
                                            ->required()
                                            ->hidden(fn($record) => $record && $record->hasRole('super-administrateur')),
                                  ])
                       ])->columns(2)
                       ->columnSpan(['lg' => fn(?User $record) => $record === null ? 4 : 3]),
                Section::make()
                       ->schema([
                           Placeholder::make('created_at')
                                      ->label(__('created_at'))
                                      ->content(fn(User $record): ?string => $record->created_at->format('j/m/Y H:i')),

                           Placeholder::make('updated_at')
                                      ->label(__('Last_modified_at'))
                                      ->content(fn(User $record): ?string => $record->updated_at->format('j/m/Y H:i')),
                       ])
                       ->columnSpan(['lg' => 1])
                       ->hidden(fn(?User $record) => $record === null),
            ])->columns(4);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('user_name')
                      ->label(__('Username'))
                      ->searchable(),
            TextColumn::make('first_name')
                      ->label(__('FirstName'))
                      ->searchable(),
            TextColumn::make('name')
                      ->label(__('LastName'))
                      ->searchable(),
            TextColumn::make('email')
                      ->label(__('Email'))
                      ->icon('heroicon-m-envelope')
                      ->searchable(),
            TextColumn::make('phone_number')
                      ->label(__('phone_number'))
                      ->toggleable(isToggledHiddenByDefault: true),
            TextColumn::make('gsm_number')
                      ->label(__('gsm number'))
                      ->toggleable(isToggledHiddenByDefault: true),
            TextColumn::make('roles.name')
                      ->label(__('Role'))
                      ->sortable(),
            TextColumn::make('created_at')
                      ->dateTime()
                      ->sortable()
                      ->toggleable(isToggledHiddenByDefault: true),
            TextColumn::make('updated_at')
                      ->dateTime()
                      ->sortable()
                      ->toggleable(isToggledHiddenByDefault: true),
        ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }

    public static function getModelLabel(): string
    {
        return __('Admin User');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Admin Users');
    }
}
