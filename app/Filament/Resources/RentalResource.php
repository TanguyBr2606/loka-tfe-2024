<?php

namespace App\Filament\Resources;

use App\Enums\RentalStatus;
use App\Filament\Resources\RentalResource\Pages;
use App\Filament\Resources\RentalResource\RelationManagers;
use App\Models\Customer;
use App\Models\Rental;
use Exception;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\ToggleButtons;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;

class RentalResource extends Resource
{
    protected static ?string $model           = Rental::class;
    protected static ?string $navigationIcon  = 'heroicon-o-shopping-cart';
    protected static ?string $navigationGroup = 'Shop';
    protected static ?string $slug            = 'rentals';

    /**
     * @throws Exception
     */
    public
    static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('status')
                          ->label('Statut')
                          ->badge(),
                TextColumn::make('number')
                          ->label(__('Rental_number'))
                          ->sortable()
                          ->searchable(),
                TextColumn::make('customer_full_name')
                          ->label(__('customer name'))
                          ->sortable(),
                TextColumn::make('products')
                          ->label('Produits et Quantités')
                          ->formatStateUsing(function ($record) {
                              return $record->products->map(function ($product) {
                                  return $product->name . ' (Qté: ' . $product->pivot->quantity . ')';
                              })->join(', ');
                          }),
                TextColumn::make('start_date')
                          ->label(__('startDateLocation'))
                          ->sortable()
                          ->date('j-m-y'),
                TextColumn::make('end_date')
                          ->label(__('endDateLocation'))
                          ->sortable()
                          ->date('j-m-y'),
                TextColumn::make('total_rental_price')
                          ->label(__('Total_rental_price'))
                          ->money('EUR')
                          ->toggleable(),
                TextColumn::make('total_deposit')
                          ->label(__('Total_deposit'))
                          ->money('EUR')
                          ->toggleable(),
                TextColumn::make('payment_type')
                          ->label('Type de paiement')
                          ->formatStateUsing(function ($state) {
                              return match ($state) {
                                  'enlevement' => 'À l\'enlèvement',
                                  'virement'   => 'Virement bancaire',
                              };
                          })
                          ->disabled()
                          ->toggleable(isToggledHiddenByDefault: true),
                TextColumn::make('created_at')
                          ->label(__('created_at'))
                          ->dateTime('j/m/Y, H:i')
                          ->sortable()
                          ->toggleable(isToggledHiddenByDefault: true),
                TextColumn::make('updated_at')
                          ->label(__('updated_at'))
                          ->dateTime('j/m/Y, H:i')
                          ->sortable()
                          ->toggleable(isToggledHiddenByDefault: true),
            ])->filters([
                SelectFilter::make('status')
                            ->options(RentalStatus::class),
                SelectFilter::make('payment_type')
                            ->label('Type de paiement')
                            ->options([
                                'enlevement' => 'A l\'enlèvement',
                                'virement' => 'Par virement bancaire',
                            ]),
                SelectFilter::make('is_paid')
                            ->label(__('is_paid'))
                            ->options([
                                '0' => 'Non Payé',
                                '1' => 'Payé',
                            ]),
                Filter::make('search')
                      ->form([
                          TextInput::make('search')
                                   ->label('Recherche')
                                   ->placeholder('Rechercher le nom ou prénom du Client')
                                   ->live()
                      ])
                      ->query(function ($query, $data) {
                          if (isset($data['search'])) {
                              $search = $data['search'];
                              $query->whereHas('customer', function ($q) use ($search) {
                                  $q->where('name', 'like', "%{$search}%")
                                    ->orWhere('firstname', 'like', "%{$search}%");
                              });
                          }
                      }),
            ]);
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make('Détails de la demande')
                       ->schema([
                           TextInput::make('number')
                                    ->label(__('Rental_number'))
                                    ->disabled()
                                    ->maxLength(25)
                                    ->unique(Rental::class, 'number', ignoreRecord: true),
                           Select::make('customer_id')
                                 ->label(__('customer name'))
                                 ->options(function () {
                                     return Customer::query()
                                                    ->select('id', 'firstname', 'name')
                                                    ->get()
                                                    ->mapWithKeys(function ($customer) {
                                                        return [$customer->id => $customer->firstname . ' ' . $customer->name];
                                                    });
                                 })
                                 ->searchable()
                                 ->disabled(fn(?Rental $record) => $record != null),
                           DatePicker::make('start_date')
                                     ->label(__('startDateLocation'))
                                     ->required()
                                     ->disabled(fn(?Rental $record) => $record != null),
                           DatePicker::make('end_date')
                                     ->label(__('endDateLocation'))
                                     ->required()
                                     ->disabled(fn(?Rental $record) => $record != null),
                           ToggleButtons::make('status')
                                        ->label('Status')
                                        ->inline()
                                        ->options(RentalStatus::class)
                                        ->default(RentalStatus::pending),
                           Textarea::make('customer_comment')
                                   ->label('Commentaire du client')
                                   ->nullable()
                                   ->autosize()
                                   ->maxLength(250)
                                   ->disabled(fn(?Rental $record) => $record != null),
                           TextInput::make('total_rental_price')
                                    ->label(__('Total_rental_price'))
                                    ->numeric()
                                    ->suffixIcon('heroicon-s-currency-euro')
                                    ->required()
                                    ->disabled(fn(?Rental $record) => $record != null),
                           TextInput::make('total_deposit')
                                    ->label(__('Total_deposit'))
                                    ->numeric()
                                    ->suffixIcon('heroicon-s-currency-euro')
                                    ->required()
                                    ->disabled(fn(?Rental $record) => $record != null),
                           Select::make('payment_type')
                                 ->label(__('type of payment selected'))
                                 ->options([
                                     'enlevement' => 'A l\'enlèvement',
                                     'virement' => 'Après validation par virement bancaire',
                                 ])
                                 ->default(0)
                                 ->disabled(fn(?Rental $record) => $record != null),
                           TextInput::make('bank_reference')
                                    ->label(__('banking_communication'))
                                    ->maxLength(50)
                                    ->default('number')
                                    ->disabled()
                                    ->hidden(function ($get) {
                                        return $get('status') == 'rejected' || $get('payment_type') == 'enlevement';
                                    }),
                           ToggleButtons::make('is_paid')
                                        ->label(__('is_paid'))
                                        ->inline()
                                        ->options([
                                            0 => "Non Payé",
                                            1 => "Payé"
                                        ])
                                        ->colors([
                                            0 => 'danger',
                                            1 => 'success',
                                        ])
                                        ->hidden(function ($get) {
                                            return $get('status') == 'rejected';
                                        }),
                           Textarea::make('admin_message')
                                   ->label('Message de l\'administrateur')
                                   ->nullable()
                                   ->maxLength(250)
                                   ->visible(fn($get) => $get('status') == 'rejected'),
                       ])
                       ->columns(2)
                       ->columnSpan(['lg' => fn(?Rental $record) => $record === null ? 4 : 3]),
                Section::make()
                       ->schema([
                           Placeholder::make('created_at')
                                      ->label(__('created_at'))
                                      ->content(fn(Rental $record): ?string => $record->created_at->format('j/m/Y H:i')),

                           Placeholder::make('updated_at')
                                      ->label(__('Last_modified_at'))
                                      ->content(fn(Rental $record): ?string => $record->updated_at->format('j/m/Y H:i')),])
                       ->columnSpan(['lg' => 1])
                       ->hidden(fn(?Rental $record) => $record === null),
                /*        Section::make('Produits demandés')
                               ->schema([
                                   Repeater::make('products')
                                           ->relationship()
                                           ->schema([
                                               Select::make('product_id')
                                                     ->label('Produit')
                                                     ->options(Product::all()->pluck('name', 'id'))
                                                     ->required()
                                                     ->reactive()
                                                     ->afterStateUpdated(function ($state,
                                                         callable $set) {
                                                         $product = Product::find($state);
                                                         if ($product) {
                                                             $set('price_location', $product->price_location);
                                                             $set('price_caution', $product->price_caution);
                                                         }
                                                     })
                                                     ->distinct()
                                                     ->disableOptionsWhenSelectedInSiblingRepeaterItems(),
                                               TextInput::make('pivot.quantity')
                                                        ->label('Quantité demandée')
                                                        ->numeric()
                                                        ->default(1)
                                                        ->required(),
                                               TextInput::make('price_location')
                                                        ->label('Prix Caution')
                                                        ->disabled()
                                                        ->dehydrated()
                                                        ->numeric()
                                                        ->suffixIcon('heroicon-s-currency-euro')
                                                        ->required(),
                                               TextInput::make('price_caution')
                                                        ->label('Prix Caution')
                                                        ->numeric()
                                                        ->disabled()
                                                        ->dehydrated()
                                                        ->suffixIcon('heroicon-s-currency-euro')
                                                        ->required(),
                                           ])
                                           ->columns(4)
                                           ->required()
                               ])
                               ->columns(2)
                               ->columnSpan(['lg' => fn(?Rental $record) => $record === null ? 4 : 3]),*/
            ])->columns(4);
    }

    public
    static function getPages(): array
    {
        return [
            'index' => Pages\ListRentals::route('/'),
            'create' => Pages\CreateRental::route('/create'),
            'edit' => Pages\EditRental::route('/{record}/edit'),
        ];
    }

    public
    static function getRelations(): array
    {
        return [
            RelationManagers\ProductsRelationManager::class,
        ];
    }

    public
    static function getNavigationLabel(): string
    {
        return __('Rental request');
    }

    public
    static function getPluralModelLabel(): string
    {
        return __('Rental requests');
    }
}
