<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ProductResource\Pages;
use App\Models\Product;
use Exception;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;

class ProductResource extends Resource
{
    protected static ?string $model           = Product::class;
    protected static ?string $navigationIcon  = 'heroicon-o-squares-2x2';
    protected static ?string $navigationGroup = 'Shop';

    public static function form(Form $form): Form
    {
        return $form->schema([
            Section::make(__('Product Information'))
                   ->schema([
                       TextInput::make('name')
                                ->label('Nom du produit')
                                ->required()
                                ->maxLength(50)
                                ->disabled(fn(?Product $record) => $record != null),
                       Textarea::make('description')
                               ->maxLength(255),
                       TextInput::make('price_location')
                                ->label(__('price_location'))
                                ->numeric()
                                ->required()
                                ->suffixIcon('heroicon-s-currency-euro'),
                       TextInput::make('price_caution')
                                ->label(__('price_caution'))
                                ->numeric()
                                ->required()
                                ->suffixIcon('heroicon-s-currency-euro'),
                       TextInput::make('quantity')
                                ->label(__('quantity'))
                                ->numeric()
                                ->required()
                                ->minValue(1),
                       Select::make('disponibility')
                             ->label(__('Disponibilité'))
                             ->options([
                                 '0' => 'Indisponible',
                                 '1' => 'Disponible',
                             ]),
                       FileUpload::make('image')
                                 ->label('Image Produit')
                                 ->image()
                                 ->directory('product')  // Le dossier où les images seront stockées
                                 ->visibility('public')   // Le stockage public
                                 ->hidden(fn(?Product $record) => $record != null)
                                 ->preserveFilenames(),
                   ])
                   ->columns(2)
                   ->columnSpan(['lg' => fn(?Product $record) => $record === null ? 3 : 2]),
            Section::make()
                   ->schema([
                       Placeholder::make('created_at')
                                  ->label(__('created_at'))
                                  ->content(fn(Product $record): ?string => $record->created_at->format('j/m/Y H:i')),

                       Placeholder::make('updated_at')
                                  ->label(__('Last_modified_at'))
                                  ->content(fn(Product $record): ?string => $record->updated_at->format('j/m/Y H:i')),
                   ])
                   ->columnSpan(['lg' => 1])
                   ->hidden(fn(?Product $record) => $record === null),
        ])->columns(3);
    }

    /**
     * @throws Exception
     */
    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('name')
                      ->label('Nom du produit')
                      ->sortable()
                      ->searchable(),
            TextColumn::make('price_location')
                      ->label(__('price_caution'))
                      ->money('EUR'),
            TextColumn::make('price_caution')
                      ->label(__('price_caution'))
                      ->money('EUR'),
            IconColumn::make('disponibility')
                      ->label(__('disponibility'))
                      ->boolean()
                      ->sortable(),
            TextColumn::make('created_at')
                      ->label(__('created_at'))
                      ->dateTime('j/m/Y, H:i')
                      ->sortable()
                      ->toggleable(isToggledHiddenByDefault: true),
            TextColumn::make('updated_at')
                      ->label(__('updated_at'))
                      ->dateTime('j/m/Y, H:i')
                      ->sortable()
                      ->toggleable(isToggledHiddenByDefault: true),
        ])->filters([

        ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProducts::route('/'),
            'create' => Pages\CreateProduct::route('/create'),
            'edit' => Pages\EditProduct::route('/{record}/edit'),
        ];
    }

    public static function getModelLabel(): string
    {
        return __('Product');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Products');
    }
}
