<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ContactResource\Pages;
use App\Models\Contact;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;

class ContactResource extends Resource
{
    protected static ?string $model           = Contact::class;
    protected static ?string $navigationIcon  = 'heroicon-o-envelope';
    protected static ?string $navigationGroup = 'Contacts';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('Contact Information'))
                       ->schema([
                           TextInput::make('subject')
                                    ->label(__('Subject'))
                                    ->disabled()
                                    ->columnSpan(2),
                           Select::make('customer_id')
                                 ->relationship('customer', 'username')
                                 ->label(__('User'))
                                 ->disabled(),
                           TextInput::make('email')
                                    ->label(__('Email'))
                                    ->disabled(),
                           TextInput::make('lastname')
                                    ->label(__('LastName'))
                                    ->disabled(),
                           TextInput::make('firstname')
                                    ->label(__('FirstName'))
                                    ->disabled(),
                           Textarea::make('message')
                                   ->label('Message')
                                   ->disabled(),
                           Textarea::make('response')
                                   ->label(__('Response'))
                                   ->placeholder('Entrer la réponse ici...')
                                   ->required(false), // Ce champ peut être modifié
                       ])->columns(2)
                       ->columnSpan(['lg' => fn(?Contact $record) => $record === null ? 3 : 2]),
                Section::make()
                       ->schema([
                           Placeholder::make('created_at')
                                      ->label(__('submitted_at'))
                                      ->content(fn(Contact $record): ?string => $record->created_at->format('j/m/Y H:i')),

                           Placeholder::make('updated_at')
                                      ->label(__('Last_modified_at'))
                                      ->content(fn(Contact $record): ?string => $record->updated_at->format('j/m/Y H:i')),
                       ])
                       ->columnSpan(['lg' => 1])
                       ->hidden(fn(?Contact $record) => $record === null),
            ])
            ->columns(3);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('subject')
                          ->label(__('Subject')),
                TextColumn::make('email')
                          ->label(__('Email'))
                          ->searchable()
                          ->sortable(),
                TextColumn::make('lastname')
                          ->label(__('LastName'))
                          ->searchable(),
                TextColumn::make('firstname')
                          ->label(__('FirstName'))
                          ->searchable(),
                TextColumn::make('message')
                          ->label(__('Message')),
                TextColumn::make('response')
                          ->label(__('Response')),
                TextColumn::make('created_at')
                          ->label(__('submitted_at'))
                          ->sortable()
                          ->dateTime("j-m-Y, H:m"),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListContacts::route('/'),
            'create' => Pages\CreateContact::route('/create'),
            'edit' => Pages\EditContact::route('/{record}/edit'),
        ];
    }
}
