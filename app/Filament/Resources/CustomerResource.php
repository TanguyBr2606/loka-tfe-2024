<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CustomerResource\Pages;
use App\Filament\Resources\CustomerResource\RelationManagers;
use App\Models\Customer;
use Carbon\Carbon;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;

class CustomerResource extends Resource
{
    protected static ?string $model           = Customer::class;
    protected static ?string $navigationIcon  = 'heroicon-m-user-group';
    protected static ?string $navigationGroup = 'Shop';
    protected static ?int    $navigationSort  = 0;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make(__('Client Information'))
                       ->schema([
                           TextInput::make('username')
                                    ->label(__('Username'))
                                    ->required()
                                    ->unique(ignoreRecord: true)
                                    ->columnSpan(2)
                                    ->disabled(fn(?Customer $record) => $record != null),
                           TextInput::make('firstname')
                                    ->label(__('FirstName'))
                                    ->regex('^[a-zA-Z\s\',\-]+$^')
                                    ->minLength(3)
                                    ->required()
                                    ->disabled(fn(?Customer $record) => $record != null),
                           TextInput::make('name')
                                    ->label(__('LastName'))
                                    ->regex('^[a-zA-Z\s\',\-]+$^')
                                    ->minLength(3)
                                    ->required()
                                    ->disabled(fn(?Customer $record) => $record != null),
                           DatePicker::make('dateOfBirthday')
                                     ->label(__('dateOfBirthday'))
                                     ->required()
                                     ->date()
                                     ->maxDate(Carbon::now()->subYears(18))
                                     ->rule('before_or_equal:' . Carbon::now()->subYears(18)->format('Y-m-d'))
                                     ->disabled(fn(?Customer $record) => $record != null),
                           Placeholder::make(__('ageRestrictionMessage'))
                                      ->content(function () {
                                          $yearLimit = Carbon::now()->subYears(18)->year;
                                          return "Vous devez être né(e) avant ou en $yearLimit pour créer un compte.";
                                      }),
                           Section::make(__('Contact Information'))
                                  ->schema([
                                      TextInput::make('email')
                                               ->label(__('Email'))
                                               ->email()
                                               ->unique()
                                               ->required()
                                               ->disabled(fn(?Customer $record) => $record != null),
                                      TextInput::make('password')
                                               ->label(__('Password'))
                                               ->password()
                                               ->revealable(filament()->arePasswordsRevealable())
                                               ->required()
                                               ->hidden(fn(?Customer $record) => $record != null),
                                      TextInput::make('phone_number')
                                               ->label(__('phone_number'))
                                               ->tel()
                                               ->telregex('/^\+(32[1-9][0-9]{7}|33[1-9][0-9]{8})$/')
                                               ->disabled(fn(?Customer $record) => $record != null),
                                      TextInput::make('gsm_number')
                                               ->label(__('gsm_number'))
                                               ->tel()
                                               ->telregex('/^\+(324[5-9][0-9]{7}|33[6-7][0-9]{8})$/')
                                               ->disabled(fn(?Customer $record) => $record != null),
                                      Placeholder::make(__('Message for Format number'))
                                                 ->content('Uniquement numéro belge ou française'),
                                  ])->columns(2),
                           Section::make(__('Address'))
                                  ->schema([
                                      TextInput::make('address')
                                               ->label(__('Adresse'))
                                               ->maxLength(50)
                                               ->disabled(fn(?Customer $record) => $record != null),
                                      TextInput::make('zip_code')
                                               ->label(__('Code postal'))
                                               ->maxLength(50)
                                               ->disabled(fn(?Customer $record) => $record != null),
                                      TextInput::make('city')
                                               ->label(__('Ville'))
                                               ->maxLength(50)
                                               ->disabled(fn(?Customer $record) => $record != null),
                                      Select::make('country')
                                            ->label(__('Pays'))
                                            ->options([
                                                'france' => 'France',
                                                'belgique' => 'Belgique',
                                            ])
                                            ->disabled(fn(?Customer $record) => $record != null),
                                  ])->columns(2)
                       ])
                       ->columns(2)
                       ->columnSpan(['lg' => fn(?Customer $record) => $record === null ? 4 : 3]),
                Section::make()
                       ->schema([
                           Placeholder::make('created_at')
                                      ->label(__('created_at'))
                                      ->content(fn(Customer $record): ?string => $record->created_at->format('j/m/Y H:i')),

                           Placeholder::make('updated_at')
                                      ->label(__('Last_modified_at'))
                                      ->content(fn(Customer $record): ?string => $record->updated_at->format('j/m/Y H:i')),
                       ])
                       ->columnSpan(['lg' => 1])
                       ->hidden(fn(?Customer $record) => $record === null),
            ])->columns(4);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                          ->label(__('LastName'))
                          ->searchable()
                          ->sortable(),
                TextColumn::make('firstname')
                          ->label(__('FirstName'))
                          ->searchable()
                          ->sortable(),
                TextColumn::make('email')
                          ->label(__('Email'))
                          ->icon('heroicon-m-envelope')
                          ->searchable()
                          ->sortable(),
                TextColumn::make('phone_number')
                          ->label(__('phone_number'))
                          ->searchable()
                          ->toggleable(),
                TextColumn::make('gsm_number')
                          ->label(__('gsm_number'))
                          ->searchable()
                          ->toggleable(),
                TextColumn::make('address')
                          ->label(__('Address'))
                          ->html()
                          ->formatStateUsing(function ($record) {
                              return '<p>' . $record->address . '</p>'
                                  . '<p>' . $record->zip_code . ' ' . $record->city . ', ' . $record->country . '</p>';
                          })
                          ->searchable(['address', 'zip_code', 'city', 'country']),
                TextColumn::make('created_at')
                          ->label(__('created_at'))
                          ->dateTime('j/m/Y, H:i')
                          ->toggleable(isToggledHiddenByDefault: true),
                TextColumn::make('updated_at')
                          ->label(__('updated_at'))
                          ->dateTime('j/m/Y, H:i')
                          ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([

            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCustomers::route('/'),
            'create' => Pages\CreateCustomer::route('/create'),
            'edit' => Pages\EditCustomer::route('/{record}/edit'),
        ];
    }

    public static function getModelLabel(): string
    {
        return __('Customer');
    }

    public static function getPluralModelLabel(): string
    {
        return __('Customers');
    }
}
