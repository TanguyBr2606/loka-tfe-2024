<?php

namespace App\Filament\Resources\RentalResource\RelationManagers;

use App\Models\Product;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;

class ProductsRelationManager extends RelationManager
{
    protected static string $relationship = 'products';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Repeater::make('products')
                        ->relationship()
                        ->schema([
                            Select::make('product_id')
                                  ->label('Produit')
                                  ->options(Product::all()->pluck('name', 'id'))
                                  ->required()
                                  ->reactive()
                                  ->afterStateUpdated(function ($state, callable $set) {
                                      $product = Product::find($state);
                                      if ($product) {
                                          $set('price_location', $product->price_location);
                                          $set('price_caution', $product->price_caution);
                                      }
                                  }),
                            TextInput::make('pivot.quantity') // Utilisez 'pivot.quantity' ici
                                     ->label('Quantité demandée')
                                     ->numeric()
                                     ->required(),
                            TextInput::make('price_location')
                                     ->label('Prix Location')
                                     ->disabled()
                                     ->numeric(),
                            TextInput::make('price_caution')
                                     ->label('Prix Caution')
                                     ->disabled()
                                     ->numeric(),
                        ])
                        ->columns(2)
                        ->required(),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('Produits Demandés')
            ->columns([
                TextColumn::make('name')
                          ->label('Produit'),
                TextColumn::make('pivot.quantity')
                          ->label('Quantité'),
                TextColumn::make('price_location')
                          ->label('Prix Location')
                          ->money('EUR'),
                TextColumn::make('price_caution')
                          ->label('Prix Caution')
                          ->money('EUR'),
            ]);
    }

}
