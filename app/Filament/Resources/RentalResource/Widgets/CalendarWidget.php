<?php

namespace App\Filament\Resources\RentalResource\Widgets;

use App\Filament\Resources\RentalResource;
use App\Models\Rental;
use Filament\Actions\CreateAction;
use Illuminate\Database\Eloquent\Model;
use Saade\FilamentFullCalendar\Data\EventData;
use Saade\FilamentFullCalendar\Widgets\FullCalendarWidget;

class CalendarWidget extends FullCalendarWidget
{
    public Model|string|null $model = Rental::class;

    public function fetchEvents(array $info): array
    {
        return Rental::query()
                     ->where('start_date', '>=', $info['start'])
                     ->where('end_date', '<=', $info['end'])
                     ->where('status', '=', 'approved')
                     ->get()
                     ->map(fn(Rental $rental) => EventData::make()
                                                          ->id($rental->id)
                                                          ->title('Location n°' . $rental->number)
                                                          ->start($rental->start_date)
                                                          ->end($rental->end_date)
                                                          ->url(RentalResource::getUrl('edit', ['record' => $rental]))
                     )->toArray();
    }

    public function headerActions(): array
    {
        return [
            CreateAction::make()
                        ->hidden()
        ];
    }
}
