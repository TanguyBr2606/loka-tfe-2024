<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    protected $routeMiddleware
        = [
            'auth.customer' => \App\Http\Middleware\AuthenticateCustomer::class,
        ];
}
