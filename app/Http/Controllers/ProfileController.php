<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Customer;
use DateTime;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(): View
    {
        // Récupérer l'instance du client authentifié via le modèle Customer
        $customer = Customer::find(Auth::id());

        return view('customer.profile.edit', ['customer' => $customer]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $customer = Customer::find(Auth::id());
        $customer->fill($request->validated());

        $customer->save();

        return Redirect::route('profile.edit')->with('status', 'profile-updated');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        // Récupérer l'instance du client authentifié via le modèle Customer
        $customer = Customer::find(Auth::id());

        Auth::logout();
        $customer->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }

    /**
     * Export données utilisateur en fichier format JSON
     *
     * @throws Exception
     */
    public function export(): \Illuminate\Http\Response|RedirectResponse
    {
        if (Auth::check()) {
            $user = Customer::find(Auth::id());

            $data = [
                'Nom d\'utilisateur' => $user->username,
                'Adresse Email' => $user->email,
                'Nom de Famille' => ucfirst($user->name),
                'Prenom' => ucfirst($user->firstname),
                'Numero de Telephone' => $user->phone_number,
                'Numero de Gsm' => $user->gsm_number,
                'Date de Naissance' => date_format(new DateTime($user->dateOfBirthday), ' d-m-Y'),
                'Adresse' => $user->address,
                'Code Postal' => $user->zip_code,
                'Ville' => $user->city,
                'Pays' => ucfirst($user->country),
            ];

            $jsonContent = json_encode($data, JSON_PRETTY_PRINT);
            $filename    = 'Data_' . $user->username . '_' . time() . '.json';

            return Response::make($jsonContent, 200, [
                'Content-Type' => 'application/json',
                'Content-Disposition' => "attachement; filename=$filename"
            ]);

        } else {
            return back()->with('error', 'L\'export n\'a pas eu lieu !');
        }
    }
}
