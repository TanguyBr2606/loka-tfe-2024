<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $cart     = session()->get('cart', []);
        $products = Product::whereIn('id', array_keys($cart))->get()->map(function ($product) use ($cart) {
            $product->quantity = $cart[$product->id]['quantity'] ?? 1;
            return $product;
        });
        return view('customer.carts.index', compact('products'));
    }

    /**
     * Store a newly created resource in session.
     */
    public function addToCart(Request $request): RedirectResponse
    {
        $productId = $request->input('product_id');
        $quantity  = 1;

        $cart = session()->get('cart', []);

        // Ajouter ou mettre à jour le produit dans le panier
        if (isset($cart[$productId])) {
            $cart[$productId]['id']       = $productId;
            $cart[$productId]['quantity'] = $quantity;
        } else {
            $cart[$productId] = ['quantity' => $quantity, 'id' => $productId];
        }

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Produit ajouté au panier.');
    }

    public function getCartInfo(): JsonResponse
    {
        $cart      = session()->get('cart', []);
        $cartCount = 0;
        $cartTotal = 0;

        foreach ($cart as $productId => $details) {
            $product = Product::find($productId);
            if ($product) {
                $cartCount += $details['quantity'];
                $cartTotal += $product->price_location * $details['quantity'];
            }
        }

        return response()->json([
            'cartCount' => $cartCount,
            'cartTotal' => number_format($cartTotal, 2)
        ]);
    }

    public function updateQuantity(Request $request, $productId): RedirectResponse
    {
        $request->validate([
            'quantity' => 'required|integer|min:1',
        ]);

        $cart = session()->get('cart', []);

        if (isset($cart[$productId])) {
            $cart[$productId]['quantity'] = $request->quantity;
            session()->put('cart', $cart);
        }

        return redirect()->route('cart.index')->with('success', 'Quantité mise à jour.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $productId): RedirectResponse
    {
        $cart = session()->get('cart', []);

        if (isset($cart[$productId])) {
            unset($cart[$productId]);
            session()->put('cart', $cart);
        }

        return redirect()->back()->with('success', 'Produit retiré du panier.');
    }
}
