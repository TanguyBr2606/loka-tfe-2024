<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        if (Auth::guard('customer')->attempt($request->only('email', 'password'))) {
            $request->session()->regenerate();

            $customer = Auth::guard('customer')->user(); // Récupération de l'utilisateur connecté en tant que Customer

            return redirect()->route('home')
                             ->with('message', 'Bienvenue ' . $customer->firstname . ' ' . $customer->name . ', vous êtes connecté.');
        } else {
            return back()->withErrors('Echec de l\'authentification');
        }
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('customer')->logout(); // Utilisation du modèle Customer pour déconnexion

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/')->with('message', 'Au revoir ! À très bientôt !!');
    }
}
