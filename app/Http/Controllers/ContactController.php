<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ContactController extends Controller
{
    public function create(): View
    {
        return view('customer.contacts.create');
    }

    public function store(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'subject' => 'required|string',
            'email' => 'required|email',
            'customer_id' => 'nullable|numeric',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'message' => 'required|string',
        ]);

        Contact::create([
            'firstname' => $validated['firstname'],
            'lastname' => $validated['lastname'],
            'email' => $validated['email'],
            'subject' => $validated['subject'],
            'message' => $validated['message'],
            'customer_id' => auth('customer')->check() ? auth('customer')->id() : null
            // Associer l'utilisateur connecté si disponible
        ]);

        return redirect()->route('contact.create')->with('success', 'Message envoyé avec succès!');
    }

    /**
     * Display the specified resource.
     *
     * Affichage des messages envoyés
     */
    public function show(): View
    {
        // Récupérez les messages de contact de l'utilisateur actuel
        $userMessages = Contact::where('customer_id', auth::guard('customer')->id())->latest()->get();

        return view('customer.contacts.show', ['userMessages' => $userMessages]);
    }
}
