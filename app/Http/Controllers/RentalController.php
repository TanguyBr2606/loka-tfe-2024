<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Rental;
use App\Models\RentalProduct;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RentalController extends Controller
{
    public function index(): View
    {
        $rentals = Rental::where('customer_id', Auth::id())->with('products')->get();

        return view('customer.rentals.index', compact('rentals'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'customer_comment' => 'nullable|string|max:500',
            'payment_type' => 'required|string|in:enlevement,virement'
        ]);

        // Vérifier si le panier n'est pas vide
        $cart = session()->get('cart', []);
        if (empty($cart)) {
            return redirect()->back()->with('error', 'Votre panier est vide.');
        } else {
            // Créer la demande de location
            $rental = Rental::create([
                'customer_id' => auth('customer')->id(),
                'customer_comment' => $request->input('comment'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'payment_type' => $request->input('payment_type'),
                'total_rental_price' => $this->calculateTotalRentalPrice($cart),
                'total_deposit' => $this->calculateTotalDeposit($cart),
                'status' => 'pending',
            ]);

            // Ajouter les produits associés à la demande
            foreach ($cart as $productId => $details) {
                RentalProduct::create([
                    'rental_id' => $rental->id,
                    'product_id' => $productId,
                    'quantity' => $details['quantity'],
                ]);
            }

            // Vider le panier de la session
            session()->forget('cart');

            return redirect()->route('cart.index')->with('success', 'Demande envoyée avec succès.');
        }
    }

    public function show($id): View
    {
        $rental = Rental::where('id', $id)->where('customer_id', Auth::id())->with('products')->firstOrFail();

        return view('customer.rentals.show', compact('rental'));
    }

    private function calculateTotalRentalPrice($cart): float|int
    {
        $total = 0;
        foreach ($cart as $productId => $details) {
            $product = Product::find($productId);
            $total   += $product->price_location * $details['quantity']; // Prix de location
        }
        return $total;
    }

    private function calculateTotalDeposit($cart): float|int
    {
        $total = 0;
        foreach ($cart as $productId => $details) {
            $product = Product::find($productId);
            $total   += $product->price_caution; // Prix de caution
        }
        return $total;
    }
}
