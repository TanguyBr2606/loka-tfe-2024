<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the customer is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'username' => 'required|string|min:4|max:50|unique:customers',
            'email' => 'required|email|unique:customers',
            'password' => 'required|string|min:5',
            'name' => 'required|string|min:3',
            'firstname' => 'required|string|min:3',
            'phone_number' => ['nullable', 'regex:/^\+(32[1-9][0-9]{7}|33[1-9][0-9]{8})$/'],
            'gsm_number' => ['nullable', 'regex:/^\+(324[5-9][0-9]{7}|33[6-7][0-9]{8})$/'],
            'dateOfBirthday' => ['required', 'date', 'before_or_equal:' . now()->subYears(18)->format('Y-m-d')],
            'address' => 'nullable|string|min:4',
            'city' => 'nullable|string|min:4',
            'zip_code' => ['nullable', 'string', 'min:4', 'max:5', 'regex:/^[0-9]{5}$|^[1-9][0-9]{3}$/'],
            'country' => 'nullable',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        $year = now()->subYears(18)->year;

        return [
            'zip_code.regex' => 'Le code postal doit être dans un format valide pour la Belgique (4 chiffres), la France (5 chiffres)',
            'dateOfBirthday.before_or_equal' => 'Vous devez fournir une date de naissance avant ou en ' . $year . ' pour vous inscrire.',
            'numTel.regex' => 'Le numéro de téléphone n\'est pas dans un format valide pour la Belgique (+32) ou la France (+33).',
            'numGsm.regex' => 'Le numéro de GSM n\'est pas dans un format valide pour la Belgique (+32 4) ou la France (+33 6-7). ',
        ];
    }
}
