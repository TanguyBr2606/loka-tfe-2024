<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the customer is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string', 'email', 'max:255',
                        Rule::unique(Customer::class)->ignore($this->user('customer')->id)],
            'name' => 'required|string|min:3',
            'firstname' => 'required|string|min:3',
            'dateOfBirthday' => ['required', 'date', 'before_or_equal:' . now()->subYears(18)->format('Y-m-d')],
            'address' => 'nullable|string',
            'city' => 'nullable|string',
            'zip_code' => ['nullable', 'string', 'min:4', 'max:5', 'regex:/^[0-9]{5}$|^[1-9][0-9]{3}$/'],
            'country' => 'nullable|string',
            'phone_number' => ['nullable', 'regex:/^\+(32[1-9][0-9]{7}|33[1-9][0-9]{8})$/'],
            'gsm_number' => ['nullable', 'regex:/^\+(324[5-9][0-9]{7}|33[6-7][0-9]{8})$/'],
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        $year = now()->subYears(18)->year;

        return [
            'zip_code.regex' => 'Le code postal doit être dans un format valide pour la Belgique (4 chiffres), la France (5 chiffres)',
            'dateOfBirthday.before_or_equal' => 'Vous devez fournir une date de naissance avant ou en ' . $year . ' dans votre profil.',
            'phone_number.regex' => 'Le numéro de téléphone n\'est pas dans un format valide pour la Belgique (+32), la France (+33).',
            'gsm_number.regex' => 'Le numéro de GSM n\'est pas dans un format valide pour la Belgique (+324), la France (+33 6 ou 7). ',
        ];
    }
}
