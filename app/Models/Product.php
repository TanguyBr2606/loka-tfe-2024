<?php

namespace App\Models;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Application;

class Product extends Model
{
    use HasFactory;

    protected $fillable
        = [
            'name',
            'price_location',
            'price_caution',
            'description',
            'quantity',
            'disponibility',
            'image'
        ];

    // Une relation pour récupérer toutes les locations dans lesquelles ce produit est inclus
    public function rental()
    {
        return $this->belongsToMany(Rental::class, 'rental_product')
                    ->withPivot(['quantity'])
                    ->withTimestamps();
    }

    public function getImageUrlAttribute(): Application|string|UrlGenerator|null
    {
        // Assure que l'image est bien définie
        if ($this->image) {
            return url('images/' . $this->image);
        }

        return null; // Ou une URL par défaut si l'image n'existe pas
    }
}
