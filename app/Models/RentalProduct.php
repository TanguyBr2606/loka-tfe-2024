<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RentalProduct extends Model
{
    use HasFactory;

    protected $table      = 'rental_product';
    public    $timestamps = true;

    protected $fillable
        = [
            'rental_id',
            'product_id',
            'quantity',
        ];

    // Relation avec Rental
    public function rental(): BelongsTo
    {
        return $this->belongsTo(Rental::class, 'rental_id', 'id');
    }

    // Relation avec Product
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
