<?php

namespace App\Models;

use App\Enums\RentalStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;

class Rental extends Model
{
    use HasFactory;

    protected $fillable
        = [
            'customer_id',
            'number',
            'customer_comment',
            'start_date',
            'end_date',
            'payment_type',
            'bank_reference',
            'admin_note',
            'total_rental_price',
            'total_deposit',
            'status',
        ];

    protected array $dates = ['start_date', 'end_date'];

    protected $casts
                       = [
            'status' => RentalStatus::class,
            'start_date' => 'date:d-m-Y',
            'end_date' => 'date:d-m-Y',
        ];
    protected $appends = ['customer_full_name'];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function products(): belongsToMany
    {
        return $this->belongsToMany(Product::class, 'rental_product')
                    ->withPivot(['quantity'])
                    ->withTimestamps();
    }

    public function getCustomerFullNameAttribute(): string
    {
        return $this->customer ? $this->customer->firstname . ' ' . $this->customer->name : '';
    }

    protected static function boot(): void
    {
        parent::boot();

        static::creating(function ($model) {
            $model->number = self::generateRentalNumber();
        });
    }

    private static function generateRentalNumber(): string
    {
        $year  = date('Y');
        $month = date('m');

        // Compter combien de locations ont été créées ce mois-ci
        $count = DB::table('rentals')
                   ->whereYear('created_at', $year)
                   ->whereMonth('created_at', $month)
                   ->count();

        // Incrémenter le nombre pour ce mois-ci
        $incrementedNumber = str_pad($count + 1, 3, '0', STR_PAD_LEFT);

        // Retourner le numéro de location formaté
        return "OR{$year}{$month}-{$incrementedNumber}";
    }
}
