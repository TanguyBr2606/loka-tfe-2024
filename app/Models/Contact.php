<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contact extends Model
{
    use HasFactory;

    protected $fillable
        = [
            'customer_id',
            'firstname',
            'lastname',
            'email',
            'subject',
            'message',
            'response'
        ];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }
}
