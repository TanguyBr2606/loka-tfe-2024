<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class installProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:project';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the project and update the .env file';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        File::copy('.env.example', '.env');

        // Informations de connexion serveur DB
        $this->info('Information for Database connection');
        $dbHost     = $this->ask('Enter the database host (Enter for default)', '127.0.0.1');
        $dbPort     = $this->ask('Enter the database port (Enter for default)', '3306');
        $dbDatabase = $this->ask('Enter the name of the database (Exemple: loka)', 'loka');
        $dbUsername = $this->ask('Enter the database username (Enter for default)', 'root');
        $dbPassword = $this->secret('Enter the database password (default: empty)');

        // Chargement du contenu dans le fichier .env
        $envContents = File::get('.env');

        // Mise à jour du fichier .env
        // preg_replace permet de rechercher et remplacer les lignes spécifiées
        $envContents = preg_replace([
            '/DB_HOST=(.*)/',
            '/DB_PORT=(.*)/',
            '/DB_DATABASE=(.*)/',
            '/DB_USERNAME=(.*)/',
            '/DB_PASSWORD=(.*)/',
        ], [
            'DB_HOST=' . $dbHost,
            'DB_PORT=' . $dbPort,
            'DB_DATABASE=' . $dbDatabase,
            'DB_USERNAME=' . $dbUsername,
            'DB_PASSWORD=' . $dbPassword,
        ], $envContents);

        // Envoie des modifications dans le fichier .env
        File::put('.env', $envContents);

        // Génération d'une nouvelle APP:KEY
        $this->call('key:generate');

        $this->info('Laravel project has been installed !');
    }
}
