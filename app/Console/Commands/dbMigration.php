<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class dbMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbMigration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to Migrate Table & Data';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Appel de la commande migrate permettant de faire la migration des tables
        $this->call('migrate');
        $this->call('db:seed');
        $this->info('Migration has successfully !');
    }
}
