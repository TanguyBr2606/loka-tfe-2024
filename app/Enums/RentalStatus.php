<?php

namespace App\Enums;

use Filament\Support\Contracts\HasColor;
use Filament\Support\Contracts\HasIcon;
use Filament\Support\Contracts\HasLabel;

enum RentalStatus: string implements HasColor, HasIcon, HasLabel
{
    case pending  = "pending";
    case approved = "approved";
    case rejected = "rejected";

    public function getLabel(): ?string
    {
        return match ($this) {
            self::pending  => "Demande en attente",
            self::approved => "Demande approuvée",
            self::rejected => "Demande rejetée",
        };
    }

    public function getColor(): string|array|null
    {
        return match ($this) {
            self::pending  => 'info',
            self::approved => 'success',
            self::rejected => 'danger',
        };
    }

    public function getBackgroundColorClass(): string
    {
        return match ($this) {
            self::pending  => 'bg-blue-300',
            self::approved => 'bg-green-300',
            self::rejected => 'bg-red-300',
        };
    }

    public function getIcon(): ?string
    {
        return match ($this) {
            self::pending  => 'heroicon-m-arrow-path',
            self::approved => 'heroicon-m-check-badge',
            self::rejected => 'heroicon-m-x-circle',
        };
    }
}
