<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $customer = [
            ['id' => 1, 'username' => 'JaneOverschueren', 'email' => 'overschueren@example.org',
             'email_verified_at' => '2024-08-26 09:54:02',
             'password' => '$2y$12$vukmRXgpH9El2u0btoId6ObW7ruhjpIytdszWiIR.oke7HuZMuBS.', 'firstname' => 'Jane',
             'name' => 'Overschueren', 'gsm_number' => '+32479850513', 'phone_number' => '+3265442585',
             'dateOfBirthday' => '1996-11-16', 'address' => 'place Heymans 8', 'city' => 'Ostende',
             'zip_code' => '8400', 'country' => 'belgique', 'remember_token' => null,
             'created_at' => '2024-08-26 09:54:02', 'updated_at' => '2024-08-26 09:54:02'],
            ['id' => 2, 'username' => 'juliette.vervoort', 'email' => 'vervoort.Juliette@example.org',
             'email_verified_at' => '2024-08-26 09:54:02',
             'password' => '$2y$12$vukmRXgpH9El2u0btoId6ObW7ruhjpIytdszWiIR.oke7HuZMuBS.', 'firstname' => 'juliette',
             'name' => 'Vervoort', 'gsm_number' => '+32496521422', 'phone_number' => '+3227081250',
             'dateOfBirthday' => '1977-06-21', 'address' => 'impasse Antoine 343', 'city' => 'Vilvoorde',
             'zip_code' => '1814', 'country' => 'belgique',
             'remember_token' => null, 'created_at' => '2024-08-26 09:54:02', 'updated_at' => '2024-08-26 09:54:02'],
            ['id' => 3, 'username' => 'Michel.Axelle', 'email' => 'axelle.michel@example.com',
             'email_verified_at' => '2024-08-26 09:54:02',
             'password' => '$2y$12$vukmRXgpH9El2u0btoId6ObW7ruhjpIytdszWiIR.oke7HuZMuBS.', 'firstname' => 'Axelle',
             'name' => 'Michel', 'gsm_number' => '+32475852618', 'phone_number' => '+3282362125',
             'dateOfBirthday' => '1996-12-09', 'address' => 'impasse Collignon 913', 'city' => 'Dinant',
             'zip_code' => '5500', 'country' => 'belgique', 'remember_token' => null,
             'created_at' => '2024-08-26 09:54:02', 'updated_at' => '2024-08-26 09:54:02'],
            ['id' => 4, 'username' => 'NicolasJast', 'email' => 'Nicolas_Jast@netbel.be',
             'email_verified_at' => '2024-08-26 09:54:02',
             'password' => '$2y$12$vukmRXgpH9El2u0btoId6ObW7ruhjpIytdszWiIR.oke7HuZMuBS.',
             'firstname' => 'Nicolas', 'name' => 'Jast', 'gsm_number' => '+32417202740',
             'phone_number' => '+3280470711', 'dateOfBirthday' => '1996-11-16', 'address' => 'Allée La Boétie 7',
             'city' => 'Mont-Saint-Guibert', 'zip_code' => '1435', 'country' => 'belgique', 'remember_token' => null,
             'created_at' => '2024-08-26 09:54:02', 'updated_at' => '2024-08-26 09:54:02'],
            ['id' => 5, 'username' => 'VivienFarrell', 'email' => 'vivienfarrell@example.org',
             'email_verified_at' => '2024-08-26 09:54:02',
             'password' => '$2y$12$vukmRXgpH9El2u0btoId6ObW7ruhjpIytdszWiIR.oke7HuZMuBS.',
             'firstname' => 'Vivien', 'name' => 'Farrell', 'gsm_number' => '+32417202740',
             'phone_number' => '+3243722274', 'dateOfBirthday' => '1996-04-22', 'address' => 'Impasse Cremin 863b',
             'city' => 'Saint-Georges-sur-Meuse', 'zip_code' => '4470', 'country' => 'belgique',
             'remember_token' => null, 'created_at' => '2024-08-26 09:54:02', 'updated_at' => '2024-08-26 09:54:02'],
        ];

        foreach ($customer as $item => $value) {
            DB::table('customers')->insert([$item => $value]);
        }
    }
}
