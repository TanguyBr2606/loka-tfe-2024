<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //User
        Permission::create(['guard_name' => 'web', 'name' => 'create users']);
        Permission::create(['guard_name' => 'web', 'name' => 'read users']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit users']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete users']);

        //Product
        Permission::create(['guard_name' => 'web', 'name' => 'create products']);
        Permission::create(['guard_name' => 'web', 'name' => 'read products']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit products']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete products']);

        //Rental
        Permission::create(['guard_name' => 'web', 'name' => 'create rentals']);
        Permission::create(['guard_name' => 'web', 'name' => 'read rentals']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit rentals']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete rentals']);

        //Contact
        Permission::create(['guard_name' => 'web', 'name' => 'create contacts']);
        Permission::create(['guard_name' => 'web', 'name' => 'read contacts']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit contacts']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete contacts']);

        //Customer
        Permission::create(['guard_name' => 'web', 'name' => 'create customers']);
        Permission::create(['guard_name' => 'web', 'name' => 'read customers']);
        Permission::create(['guard_name' => 'web', 'name' => 'edit customers']);
        Permission::create(['guard_name' => 'web', 'name' => 'delete customers']);

        /* Assignation des permissions par roles */
        Role::create(['guard_name' => 'web', 'name' => 'super-administrateur'])->givePermissionTo([
            'create users', 'read users', 'edit users', 'delete users',
            'create products', 'read products', 'edit products', 'delete products',
            'create rentals', 'read rentals', 'edit rentals', 'delete rentals',
            'create contacts', 'read contacts', 'edit contacts', 'delete contacts',
            'create customers', 'read customers', 'edit customers', 'delete customers',
        ])->guard(['web']);

        Role::create(['guard_name' => 'web', 'name' => 'administrateur'])->givePermissionTo([
            'create users', 'read users', 'edit users', 'delete users',
            'create products', 'read products', 'edit products', 'delete products',
            'read rentals', 'edit rentals', 'delete rentals',
            'read contacts', 'edit contacts', 'delete contacts',
            'create customers', 'read customers', 'edit customers', 'delete customers',
        ])->guard(['web']);

        User::create([
            'user_name' => "tanguybr",
            'first_name' => "Tanguy",
            'name' => "Brondelet",
            'email' => "tanguybrondelet@outlook.be",
            'password' => Hash::make('admin'),
        ])->assignRole('super-administrateur');

        User::create([
            'user_name' => "JohnDoe",
            'first_name' => "John",
            'name' => "Doe",
            'email' => "admin@outlook.be",
            'password' => Hash::make('admin'),
        ])->assignRole('administrateur');

    }
}
