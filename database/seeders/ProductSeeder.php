<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $product = [
            ['id' => 1, 'name' => 'Tonnelle', 'price_caution' => 200.00, 'price_location' => 120.00,
             'description' => '5m x 8m', 'disponibility' => 1, 'quantity' => 1, 'image' => 'product/1_tonnelle_5x8.jpg',
             'created_at' => '2023-12-08 12:49:43', 'updated_at' => '2024-01-25 13:25:51'],
            ['id' => 2, 'name' => 'Samovar', 'price_caution' => 50.00, 'price_location' => 20.00,
             'description' => '15 Litres', 'disponibility' => 0, 'quantity' => 1, 'image' => 'product/2_Samovar.png',
             'created_at' => '2023-12-08 12:51:07', 'updated_at' => '2023-12-08 12:51:07'],
            ['id' => 3, 'name' => 'Bain Marie', 'price_caution' => 50.00, 'price_location' => 25.00,
             'description' => NULL, 'disponibility' => 1, 'quantity' => 1, 'image' => 'product/3_BainMarie.png',
             'created_at' => '2023-12-08 13:57:10', 'updated_at' => '2023-12-08 13:57:10'],
            ['id' => 4, 'name' => 'Cuiseur à Pâtes', 'price_caution' => 50.00, 'price_location' => 30.00,
             'description' => NULL, 'disponibility' => 1, 'quantity' => 1, 'image' => 'product/4_CuiseurPates.png',
             'created_at' => '2023-12-08 14:45:12', 'updated_at' => '2024-01-25 17:48:31'],
            ['id' => 5, 'name' => 'Chaise', 'price_caution' => 2.00, 'price_location' => 1.00,
             'description' => 'Chaises blanche en plastique pliante', 'disponibility' => 1, 'quantity' => 4,
             'image' => 'product/5_chaises.png', 'created_at' => '2024-01-25 14:58:24',
             'updated_at' => '2024-01-25 17:59:30'],
            ['id' => 6, 'name' => 'Table Pliante', 'price_caution' => 15.00, 'price_location' => 8.00,
             'description' => NULL, 'disponibility' => 1, 'quantity' => 1, 'image' => 'product/6_table_pliante.png',
             'created_at' => '2024-01-25 14:58:24', 'updated_at' => '2024-01-25 17:59:30'],
            ['id' => 7, 'name' => 'Table Mange Debout', 'price_caution' => 15.00, 'price_location' => 7.00,
             'description' => NULL, 'disponibility' => 1, 'quantity' => 1, 'image' => 'product/7_mange_debout.png',
             'created_at' => '2024-01-25 14:58:24', 'updated_at' => '2024-01-25 17:59:30'],
            ['id' => 8, 'name' => 'Housse Table Mange-Debout', 'price_caution' => 6.00, 'price_location' => 3.00,
             'description' => 'Housse Noires ou Blanches', 'disponibility' => 1, 'quantity' => 2,
             'image' => 'product/8_housseMangeDebout.png', 'created_at' => '2024-01-25 14:58:24',
             'updated_at' => '2024-01-25 17:59:30'],
            ['id' => 9, 'name' => 'Housse de Chaise', 'price_caution' => 2.00, 'price_location' => 1.00,
             'description' => NULL, 'disponibility' => 1, 'quantity' => 8, 'image' => 'product/9_Housse_Chaise.png',
             'created_at' => '2024-01-25 18:09:23', 'updated_at' => '2024-01-25 18:09:23'],
            ['id' => 10, 'name' => 'Appareil à raclette', 'price_caution' => 30.00, 'price_location' => 15.00,
             'description' => NULL, 'disponibility' => 1, 'quantity' => 1, 'image' => 'product/10_appareilRaclette.png',
             'created_at' => '2024-01-25 15:49:44', 'updated_at' => '2024-01-25 18:00:11'],
            ['id' => 11, 'name' => 'Appareil à Hotdog', 'price_caution' => 30.00, 'price_location' => 25.00,
             'description' => NULL, 'disponibility' => 0, 'quantity' => 1, 'image' => 'product/11_Appareil_Hotdog.png',
             'created_at' => '2024-01-25 15:49:44', 'updated_at' => '2024-01-25 18:00:11'],
            ['id' => 12, 'name' => 'Appareil Barbe à papa', 'price_caution' => 50.00, 'price_location' => 30.00,
             'description' => NULL, 'disponibility' => 0, 'quantity' => 1,
             'image' => 'product/12_appareilBarbeaPapa.png', 'created_at' => '2024-01-25 18:00:27',
             'updated_at' => '2024-01-25 18:00:27'],
            ['id' => 13, 'name' => 'Wok Electrique', 'price_caution' => 50.00, 'price_location' => 30.00,
             'description' => NULL, 'disponibility' => 0, 'quantity' => 1, 'image' => 'product/13_wokElectrique.png',
             'created_at' => '2024-01-25 18:00:27', 'updated_at' => '2024-01-25 18:00:27'],
            ['id' => 14, 'name' => 'Distributeur Boisson Chaude', 'price_caution' => 30.00, 'price_location' => 15.00,
             'description' => '28 litres', 'disponibility' => 1, 'quantity' => 1,
             'image' => 'product/14_distributeurBoissonChaude.png', 'created_at' => '2024-01-25 18:00:27',
             'updated_at' => '2024-01-25 18:00:27'],
            ['id' => 15, 'name' => 'Candy Bar', 'price_caution' => 50.00, 'price_location' => 40.00,
             'description' => NULL, 'disponibility' => 1, 'quantity' => 1, 'image' => 'product/15_candy_Bar.jpg',
             'created_at' => '2024-01-25 18:09:23', 'updated_at' => '2024-01-25 18:09:23'],
            ['id' => 16, 'name' => 'Arche à Ballon', 'price_caution' => 30.00, 'price_location' => 25.00,
             'description' => '18 cm', 'disponibility' => 0, 'quantity' => 1, 'image' => 'product/16_Arche.jpg',
             'created_at' => '2024-01-25 14:58:24', 'updated_at' => '2024-01-25 17:59:30'],
            ['id' => 17, 'name' => 'Chauffe Terrasse', 'price_caution' => 50.00, 'price_location' => 25.00,
             'description' => NULL, 'disponibility' => 0, 'quantity' => 1, 'image' => 'product/17_chauffe_terrasse.png',
             'created_at' => '2024-01-25 18:09:23', 'updated_at' => '2024-01-25 18:09:23']
        ];

        foreach ($product as $item => $value) {
            DB::table('products')->insert([$item => $value]);
        }
    }
}
