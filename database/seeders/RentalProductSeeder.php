<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RentalProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $rental_product = [
            ['id' => 1, 'rental_id' => 1, 'product_id' => 1, 'quantity' => 1, 'created_at' => '2024-06-25 19:28:47',
             'updated_at' => '2024-06-25 19:28:47'],
            ['id' => 2, 'rental_id' => 2, 'product_id' => 5, 'quantity' => 4, 'created_at' => '2024-06-30 17:28:47',
             'updated_at' => '2024-06-30 17:28:47'],
            ['id' => 3, 'rental_id' => 2, 'product_id' => 9, 'quantity' => 4, 'created_at' => '2024-06-30 17:28:47',
             'updated_at' => '2024-06-30 17:28:47'],
            ['id' => 4, 'rental_id' => 2, 'product_id' => 7, 'quantity' => 1, 'created_at' => '2024-06-30 17:28:47',
             'updated_at' => '2024-06-30 17:28:47'],
            ['id' => 5, 'rental_id' => 2, 'product_id' => 8, 'quantity' => 1, 'created_at' => '2024-06-30 17:28:47',
             'updated_at' => '2024-06-30 17:28:47'],
            ['id' => 6, 'rental_id' => 3, 'product_id' => 11, 'quantity' => 1, 'created_at' => '2024-07-30 17:28:47',
             'updated_at' => '2024-06-25 19:28:47'],
            ['id' => 7, 'rental_id' => 3, 'product_id' => 12, 'quantity' => 1, 'created_at' => '2024-07-30 17:28:47',
             'updated_at' => '2024-06-25 19:28:47'],
            ['id' => 8, 'rental_id' => 4, 'product_id' => 2, 'quantity' => 1, 'created_at' => '2024-08-31 17:28:47',
             'updated_at' => '2024-08-31 17:28:47'],
            ['id' => 9, 'rental_id' => 5, 'product_id' => 15, 'quantity' => 1, 'created_at' => '2024-09-01 10:25:20',
             'updated_at' => '2024-09-01 10:25:20'],
            ['id' => 10, 'rental_id' => 5, 'product_id' => 16, 'quantity' => 1, 'created_at' => '2024-09-01 10:25:20',
             'updated_at' => '2024-09-01 10:25:20'],
            ['id' => 11, 'rental_id' => 6, 'product_id' => 1, 'quantity' => 1, 'created_at' => '2024-09-02 15:58:50',
             'updated_at' => '2024-09-02 15:58:50'],
            ['id' => 12, 'rental_id' => 7, 'product_id' => 3, 'quantity' => 1, 'created_at' => '2024-09-03 08:25:30',
             'updated_at' => '2024-09-03 08:25:30'],
            ['id' => 13, 'rental_id' => 8, 'product_id' => 1, 'quantity' => 1, 'created_at' => '2024-09-04 10:45:58',
             'updated_at' => '2024-09-04 10:45:58'],
            ['id' => 14, 'rental_id' => 9, 'product_id' => 1, 'quantity' => 1, 'created_at' => '2024-09-04 14:45:58',
             'updated_at' => '2024-09-04 14:45:58'],
            ['id' => 15, 'rental_id' => 9, 'product_id' => 3, 'quantity' => 1, 'created_at' => '2024-09-04 14:45:58',
             'updated_at' => '2024-09-04 14:45:58'],
            ['id' => 16, 'rental_id' => 9, 'product_id' => 11, 'quantity' => 1, 'created_at' => '2024-09-04 14:45:58',
             'updated_at' => '2024-09-04 14:45:58'],
            ['id' => 17, 'rental_id' => 9, 'product_id' => 12, 'quantity' => 1, 'created_at' => '2024-09-04 14:45:58',
             'updated_at' => '2024-09-04 14:45:58'],
            ['id' => 18, 'rental_id' => 9, 'product_id' => 15, 'quantity' => 1, 'created_at' => '2024-09-04 14:45:58',
             'updated_at' => '2024-09-04 14:45:58'],
            ['id' => 19, 'rental_id' => 10, 'product_id' => 1, 'quantity' => 1, 'created_at' => '2024-09-04 14:45:58',
             'updated_at' => '2024-09-04 14:45:58'],
            ['id' => 20, 'rental_id' => 10, 'product_id' => 4, 'quantity' => 1, 'created_at' => '2024-09-04 18:59:58',
             'updated_at' => '2024-09-04 18:59:58'],
        ];

        foreach ($rental_product as $item => $value) {
            DB::table('rental_product')->insert([$item => $value]);
        }
    }
}
