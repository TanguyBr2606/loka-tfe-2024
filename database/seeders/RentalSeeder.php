<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RentalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $rental = [
            ['id' => 1, 'customer_id' => 1, 'number' => 'OR202406-001', 'start_date' => '2024-07-20',
             'end_date' => '2024-07-22', 'payment_type' => 'enlevement', 'total_rental_price' => '200',
             'total_deposit' => '120', 'status' => 'rejected', 'created_at' => '2024-06-25 19:28:47',
             'updated_at' => '2024-06-25 19:28:47'],
            ['id' => 2, 'customer_id' => 2, 'number' => 'OR202406-002',
             'customer_comment' => 'Housse Mange-debout Noir', 'start_date' => '2024-07-12', 'end_date' => '2024-07-15',
             'payment_type' => 'virement', 'bank_reference' => 'OR202406-002 Vervoort', 'is_paid' => 1,
             'total_rental_price' => '18', 'total_deposit' => '37', 'status' => 'approved',
             'created_at' => '2024-06-30 17:28:47', 'updated_at' => '2024-06-30 17:28:47'],
            ['id' => 3, 'customer_id' => 3, 'number' => 'OR202408-003',
             'customer_comment' => 'Consequatur numquam a tenetur dolor aut iste deleniti ut.',
             'start_date' => '2024-08-10', 'end_date' => '2024-08-12', 'payment_type' => 'enlevement', 'is_paid' => 1,
             'total_rental_price' => '55', 'total_deposit' => '80', 'status' => 'approved',
             'created_at' => '2024-07-30 17:28:47', 'updated_at' => '2024-07-30 17:28:47',],
            ['id' => 4, 'customer_id' => 4, 'number' => 'OR202408-004', 'start_date' => '2024-09-14',
             'end_date' => '2024-09-16', 'payment_type' => 'enlevement', 'total_rental_price' => '20',
             'total_deposit' => '50', 'status' => 'pending', 'created_at' => '2024-08-31 17:28:47',
             'updated_at' => '2024-08-31 17:28:47'],
            ['id' => 5, 'customer_id' => 5, 'number' => 'OR202409-001', 'start_date' => '2024-09-20',
             'end_date' => '2024-09-23', 'payment_type' => 'virement', 'bank_reference' => 'OR202409-001 Farrell',
             'total_rental_price' => '65', 'total_deposit' => '80', 'status' => 'approved',
             'created_at' => '2024-09-01 10:25:20', 'updated_at' => '2024-09-01 10:25:20'],
            ['id' => 6, 'customer_id' => 1, 'number' => 'OR202409-002', 'start_date' => '2024-09-07',
             'end_date' => '2024-09-09', 'payment_type' => 'enlevement', 'total_rental_price' => '120',
             'total_deposit' => '200', 'status' => 'rejected', 'created_at' => '2024-09-02 15:58:50',
             'updated_at' => '2024-09-02 15:58:50'],
            ['id' => 7, 'customer_id' => 2, 'number' => 'OR202409-003',
             'customer_comment' => 'Amet sit magni nam et.', 'start_date' => '2024-09-15', 'end_date' => '2024-09-16',
             'payment_type' => 'virement', 'bank_reference' => 'OR202409-003 Vervoort', 'is_paid' => 0,
             'total_rental_price' => '25', 'total_deposit' => '50', 'status' => 'pending',
             'created_at' => '2024-09-03 08:25:30', 'updated_at' => '2024-09-03 08:25:30'],
            ['id' => 8, 'customer_id' => 3, 'number' => 'OR202409-004', 'start_date' => '2024-10-05',
             'end_date' => '2024-10-07', 'payment_type' => 'enlevement', 'is_paid' => 0, 'total_rental_price' => '15',
             'total_deposit' => '30', 'status' => 'pending', 'created_at' => '2024-09-04 10:45:58',
             'updated_at' => '2024-09-04 10:45:58'],
            ['id' => 9, 'customer_id' => 4, 'number' => 'OR202409-005', 'customer_comment' => 'Pour un anniversaire',
             'start_date' => '2024-09-27', 'end_date' => '2024-09-30', 'payment_type' => 'virement',
             'bank_reference' => 'OR202409-002 Nicolas Jast', 'is_paid' => 0,
             'total_rental_price' => '200', 'total_deposit' => '330', 'status' => 'approved',
             'created_at' => '2024-09-04 14:45:58', 'updated_at' => '2024-09-04 14:45:58'],
            ['id' => 10, 'customer_id' => 5, 'number' => 'OR202409-006', 'start_date' => '2024-09-20',
             'end_date' => '2024-09-22', 'payment_type' => 'enlevement', 'is_paid' => 0,
             'admin_note' => 'Produit non disponible à ces dates', 'total_rental_price' => '150',
             'total_deposit' => '250', 'status' => 'rejected', 'created_at' => '2024-09-04 18:59:58',
             'updated_at' => '2024-09-04 18:59:58'],
        ];

        foreach ($rental as $item => $value) {
            DB::table('rentals')->insert([$item => $value]);
        }

    }
}
