<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $contact = [
            ['id' => 1, 'subject' => 'Quia voluptates voluptates reiciendis.', 'email' => 'overschueren@example.org',
             'lastname' => 'Overschueren', 'firstname' => 'Jane',
             'message' => 'Aut et quia optio culpa voluptatum aut provident. Odio possimus non officiis perferendis ut eum itaque soluta. Omnis qui soluta omnis sunt.',
             'response' => null, 'customer_id' => 2, 'created_at' => '2024-09-05 19:28:46',
             'updated_at' => '2024-09-05 19:28:46'],
            ['id' => 2, 'subject' => 'Velit tempore aliquam excepturi voluptas.',
             'email' => 'vervoort.Juliette@example.org', 'lastname' => 'Vervoort', 'firstname' => 'Juliette',
             'message' => 'Rerum recusandae repudiandae corrupti. Eum sint consectetur mollitia explicabo qui atque.',
             'response' => 'Ullam et tenetur sit et eos est.', 'customer_id' => 2,
             'created_at' => '2024-09-05 19:28:50', 'updated_at' => '2024-09-05 19:28:50'
            ],
            ['id' => 3, 'subject' => 'Maxime ea aut minima sed explicabo quia.', 'email' => 'axelle.michel@example.com',
             'lastname' => 'michel', 'firstname' => 'Axelle',
             'message' => 'Nobis temporibus ipsum in minima perspiciatis. Ab modi debitis odio ut fuga quaerat. Sed excepturi quas sequi modi.',
             'response' => null, 'customer_id' => 3, 'created_at' => '2024-09-05 19:28:50',
             'updated_at' => '2024-09-05 19:28:50'
            ],
            ['id' => 4, 'subject' => 'Sit veritatis minus doloribus repellendus.',
             'email' => 'overschueren@example.org', 'lastname' => 'Overschueren', 'firstname' => 'Jane',
             'message' => 'Aut et quia optio culpa voluptatum aut provident. Odio possimus non officiis perferendis ut eum itaque soluta. Omnis qui soluta omnis sunt.',
             'response' => 'lorem ipsum dolor sit amet consectetur adipiscing elit nisl', 'customer_id' => 1,
             'created_at' => '2024-09-02 19:28:46', 'updated_at' => '2024-09-02 19:28:46'
            ],
            ['id' => 5, 'subject' => 'Velit fugiat blanditiis in quas.', 'email' => 'Nicolas_Jast@netbel.be',
             'lastname' => 'Jast', 'firstname' => 'Nicolas',
             'message' => 'Odit quibusdam aut est officia quam quam. Voluptate vel animi veniam voluptatum occaecati. Reprehenderit sed ut aspernatur distinctio itaque qui et. Provident ipsum minus asperiores neque.',
             'response' => null, 'customer_id' => 4, 'created_at' => '2024-08-30 17:28:46',
             'updated_at' => '2024-08-30 17:28:46'
            ],
        ];

        foreach ($contact as $item => $value) {
            DB::table('contacts')->insert([$item => $value]);
        }
    }
}
