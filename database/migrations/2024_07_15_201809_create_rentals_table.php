<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')->constrained('customers')->onDelete('cascade');
            $table->string('number', 25)->unique();
            $table->text('customer_comment')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('payment_type')->default('enlevement');
            $table->string('bank_reference')->nullable();
            $table->boolean('is_paid')->default('0');
            $table->text('admin_note')->nullable();
            $table->decimal('total_rental_price', 8, 2);
            $table->decimal('total_deposit', 8, 2);
            $table->enum('status', ['pending', 'approved', 'rejected'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rentals');
    }
};
